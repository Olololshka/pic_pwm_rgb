/* 
 * File:   softPWM.h
 * Author: tolyan
 *
 * Created on 9.02.2016, 13:15
 */

#ifndef SOFTPWM_H
#define	SOFTPWM_H

#include "config.h"

#ifdef	__cplusplus
extern "C" {
#endif

    #define T3_MODULE_CLOCK            (FCY)

    typedef void (*softPwm_SetPin)(char value);

    struct SoftPWMEntry {
        unsigned char period;
        unsigned char value;
        unsigned char counter;
        softPwm_SetPin setPin;
    };

    void SoftPWMISR();

    void SoftPWM_configure(uint32_t targetBasePeriod);
    error_t SoftPWM_addPin(struct SoftPWMEntry* entry);
    error_t SoftPWM_rmPin(struct SoftPWMEntry* entry);

#ifdef	__cplusplus
}
#endif

#endif	/* SOFTPWM_H */

