/******************************************************************************/
/*Files to Include                                                            */
/******************************************************************************/

#if defined(__XC)
    #include <xc.h>         /* XC8 General Include File */
#elif defined(HI_TECH_C)
    #include <htc.h>        /* HiTech General Include File */
#elif defined(__18CXX)
    #include <p18cxxx.h>    /* C18 General Include File */
#endif

#if defined(__XC) || defined(HI_TECH_C)

#include <stdint.h>         /* For uint8_t definition */
#include <stdbool.h>        /* For true/false definition */

#endif

#include "timer.h"

void genPrescalerPostscalerPlank(uint32_t Kd, enum Prescaler* PrescalerVal,
        enum Postscaler *postscalerVal, uint8_t *plank)
{
    static const unsigned char prescalers[] = {1, 4, 16};
    enum Postscaler postscaler;
    uint8_t i;
    uint8_t _plank = 0xff;

    for (i = PR_1; i < sizeof(prescalers); ++i) {
        for (postscaler = PS_1; postscaler <= PS_16; ++postscaler) {
            uint16_t need_dev = Kd / (prescalers[i] * postscaler);
            if (need_dev < 0x100)
            {
                _plank = need_dev;
                goto __end_genPrescalerPostscalerStart;
            }
        }
    }

__end_genPrescalerPostscalerStart:
    *PrescalerVal = i;
    *postscalerVal = postscaler - 1;
    *plank = _plank;
}
