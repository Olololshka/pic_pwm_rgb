/* 
 * File:   timer.h
 * Author: tolyan
 *
 * Created on 10 ??????? 2016 ?., 8:29
 */

#ifndef TIMER_H
#define	TIMER_H

#ifdef	__cplusplus
extern "C" {
#endif

enum Prescaler {
    PR_1 = 0,
    PR_4 = 1,
    PR_16 = 2
};

enum Postscaler {
    PS_1 = 1,
    PS_2 = 2,
    PS_3 = 3,
    PS_4 = 4,
    PS_5 = 5,
    PS_6 = 6,
    PS_7 = 7,
    PS_8 = 8,
    PS_9 = 9,
    PS_10 = 10,
    PS_11 = 11,
    PS_12 = 12,
    PS_13 = 13,
    PS_14 = 14,
    PS_15 = 15,
    PS_16 = 16,
};

void genPrescalerPostscalerPlank(uint32_t Kd, enum Prescaler* PrescalerVal,
        enum Postscaler *postscalerVal, uint8_t *plank);

#ifdef	__cplusplus
}
#endif

#endif	/* TIMER_H */

