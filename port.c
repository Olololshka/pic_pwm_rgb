/******************************************************************************/
/*Files to Include                                                            */
/******************************************************************************/

#if defined(__XC)
    #include <xc.h>         /* XC8 General Include File */
#elif defined(HI_TECH_C)
    #include <htc.h>        /* HiTech General Include File */
#elif defined(__18CXX)
    #include <p18cxxx.h>    /* C18 General Include File */
#endif

#if defined(__XC) || defined(HI_TECH_C)

#include <stdint.h>         /* For uint8_t definition */
#include <stdbool.h>        /* For true/false definition */

#endif

#include "config.h"
#include "timer.h"

#include "port.h"

#if defined(__18F1320__)
#define R_PIN               (LATAbits.LA3)
#define G_PIN               (LATBbits.LB0)
#define B_PIN               (LATBbits.LB1)

#define BUTTON_VAL          (PORTBbits.RB4)

#define VALCODER_P0         (PORTBbits.RB2)
#define VALCODER_P1         (PORTBbits.RB3)
#elif defined(__18F252__)
#define R_PIN               (LATAbits.LA0)
#define G_PIN               (LATAbits.LA1)
#define B_PIN               (LATAbits.LA5)

#define BUTTON_VAL          (PORTBbits.RB3)

#define VALCODER_P0         (PORTBbits.RB1)
#define VALCODER_P1         (PORTBbits.RB2)
#endif

static shaduler_tick_routine shaduler_r = NULL;
static soft_pwm_routine soft_pwm_r = NULL;

#if defined(__18F1320__) || defined(__18F252__)
static uint16_t SPWM_TimerReloadValue = 0;
#endif

////////////////////////////////////////////////////////////////////////////////

void SetR(char value) {
#if defined(__18F1320__) || defined(__18F252__)
    PIN_SET(R_PIN, value);
#endif
}

void SetG(char value) {
#if defined(__18F1320__) || defined(__18F252__)
    PIN_SET(G_PIN, value);
#endif
}

void SetB(char value) {
#if defined(__18F1320__) || defined(__18F252__)
    PIN_SET(B_PIN, value);
#endif
}

BOOL getButtonState() {
#if defined(__18F1320__) || defined(__18F252__)
    return !BUTTON_VAL;
#endif
}

char getValcoderState() {
#if defined(__18F1320__) || defined(__18F252__)
    return ((char)VALCODER_P1) << 1 | VALCODER_P0;
#endif
}

void btnTest_r(enum ButtonActionID, void*) {
#if defined(__18F1320__)
    LATAbits.LA4 = ~LATAbits.LA4;
#elif defined(__18F252__)
    LATAbits.LA2 = ~LATAbits.LA2;
#endif
}

void valcoder_tp(void*) {
#if defined(__18F1320__)
    LATAbits.LA0 = ~LATAbits.LA0;
#elif defined(__18F252__)
    LATAbits.LA3 = ~LATAbits.LA3;
#endif
}

void valcoder_tm(void*) {
#if defined(__18F1320__)
    LATAbits.LA1 = ~LATAbits.LA1;
#elif defined(__18F252__)
    LATAbits.LA4 = ~LATAbits.LA4;
#endif
}

void DisableUnnessusaryModules() {
#if defined(__18F1320__)
    ADCON0bits.ADON = FALSE;    // disable ADC
    ADCON1 = 0b01111111;        // All pins are digital
    RCSTAbits.SPEN = FALSE;     // disable UART
    CCP1CONbits.CCP1M = FALSE;  // disable ECCP
#elif defined(__18F252__)
    ADCON0bits.ADON = FALSE;    // disable ADC
    ADCON1 = 0b01111111;        // All pins are digital
    RCSTAbits.SPEN = FALSE;     // disable UART
    CCP1CONbits.CCP1M = FALSE;  // disable ECCP
    SSPCON1bits.SSPEN = FALSE;  // disable i2c
#endif
}

void ConfigureIOPins() {
#if defined(__18F1320__)
    // Input pins
    TRISBbits.RB4 = IO_PIN_INPUT;
    TRISBbits.RB2 = IO_PIN_INPUT;
    TRISBbits.RB3 = IO_PIN_INPUT;

    // Output pins
    TRISAbits.RA3 = IO_PIN_OUTPUT; // R output
    TRISBbits.RB0 = IO_PIN_OUTPUT; // G output
    TRISBbits.RB1 = IO_PIN_OUTPUT; // B output

    R_PIN = !OUTPUT_ACTIVE_LVL;
    G_PIN = !OUTPUT_ACTIVE_LVL;
    B_PIN = !OUTPUT_ACTIVE_LVL;

#ifdef __DEBUG
    TRISAbits.RA0 = IO_PIN_OUTPUT;
    TRISAbits.RA1 = IO_PIN_OUTPUT;
    TRISAbits.RA4 = IO_PIN_OUTPUT;
#endif /* __DEBUG */

#elif defined(__18F252__)
    // Input pins
    TRISBbits.RB1 = IO_PIN_INPUT;
    TRISBbits.RB2 = IO_PIN_INPUT;
    TRISBbits.RB3 = IO_PIN_INPUT;

    // Output pins
    TRISAbits.RA0 = IO_PIN_OUTPUT; // R output
    TRISAbits.RA1 = IO_PIN_OUTPUT; // G output
    TRISAbits.RA5 = IO_PIN_OUTPUT; // B output

    R_PIN = !OUTPUT_ACTIVE_LVL;
    G_PIN = !OUTPUT_ACTIVE_LVL;
    B_PIN = !OUTPUT_ACTIVE_LVL;

#ifdef __DEBUG
    TRISAbits.RA2 = IO_PIN_OUTPUT;
    TRISAbits.RA3 = IO_PIN_OUTPUT;
    TRISAbits.RA4 = IO_PIN_OUTPUT;
#endif /* __DEBUG */
#endif
}

void configure_shaduler_timer(uint32_t targetBasePeriod,
    shaduler_tick_routine r)
{
#if defined(__18F1320__) || defined(__18F252__)
    enum Postscaler postscaler;
    enum Prescaler prescaler;

    // autoselect prescaler and postscaler
    genPrescalerPostscalerPlank(targetBasePeriod, &prescaler,
        &postscaler, (unsigned char*)&PR2);

    T2CON = 0;

    T2CONbits.TOUTPS = postscaler;
    T2CONbits.T2CKPS = prescaler;

    TMR2 = 0;

    IPR1bits.TMR2IP = 0; // low prio
    
    shaduler_r = r;
    
    PIR1bits.TMR2IF = 0;
    PIE1bits.TMR2IE = 1;

    T2CONbits.TMR2ON = 1; // enable
#endif
}

void configure_soft_pwm_timer(uint32_t targetBasePeriod,
    soft_pwm_routine r)
{
#if defined(__18F1320__) || defined(__18F252__)
    enum Prescaler calculedPrescaler;
    enum Postscaler calculedPostscaler;
    uint8_t plank;

    genPrescalerPostscalerPlank(targetBasePeriod, &calculedPrescaler,
        &calculedPostscaler, &plank);

    calculedPostscaler++;

    switch (calculedPrescaler) {
        case PR_1:
            T3CONbits.T3CKPS = calculedPrescaler;
            break;
        case PR_4:
            T3CONbits.T3CKPS = 0b10;
            break;
        case PR_16:
            T3CONbits.T3CKPS = 0b11;
            calculedPostscaler *= 2;
            break;
        default: return;
    }

    T3CONbits.RD16 = FALSE; // two 8 bit io
    T3CONbits.TMR3CS = FALSE; // F_OSC / 4

    SPWM_TimerReloadValue = 
            (uint16_t)0 - ((uint16_t)plank * calculedPostscaler);
    TMR3 = SPWM_TimerReloadValue;

    IPR2bits.TMR3IP = TRUE; // high prio

    soft_pwm_r = r;

    PIR2bits.TMR3IF = FALSE; // reset interrupt flag
    PIE2bits.TMR3IE = TRUE;
    T3CONbits.TMR3ON = TRUE;
#endif
}

////////////////////////////////////////////////////////////////////////////////

#if defined(__XC) || defined(HI_TECH_C)
void interrupt high_isr(void)
#elif defined (__18CXX)
#pragma code high_isr=0x08
#pragma interrupt high_isr
void high_isr(void)
#else
#error "Invalid compiler selection for implemented ISR routines"
#endif
{
#if defined(__18F1320__) || defined(__18F252__)
    if (PIR2bits.TMR3IF) {
        PIR2bits.TMR3IF = 0;
        TMR3 += SPWM_TimerReloadValue;
        if (soft_pwm_r)
            soft_pwm_r();
    }
#endif
}


/* Low-priority interrupt routine */
#if defined(__XC) || defined(HI_TECH_C)
void low_priority interrupt low_isr(void)
#elif defined (__18CXX)
#pragma code low_isr=0x18
#pragma interruptlow low_isr
void low_isr(void)
#else
#error "Invalid compiler selection for implemented ISR routines"
#endif
{
#if defined(__18F1320__) || defined(__18F252__)
    if (PIR1bits.TMR2IF) {
        PIR1bits.TMR2IF = 0;
        if (shaduler_r)
            shaduler_r();
    }
#endif
}

/* Refer to the device datasheet for information about available
oscillator configurations. */
void ConfigureOscillator(void)
{
#if defined(__18F1320__)
    // enable clocking peripherials after sleep
    OSCCONbits.IDLEN = TRUE;
#endif
}



/******************************************************************************/
/* Configuration Bits                                                         */
/*                                                                            */
/* Refer to 'HI-TECH PICC and PICC18 Toolchains > PICC18 Configuration        */
/* Settings' under Help > Contents in MPLAB X IDE for available PIC18         */
/* Configuration Bit Settings for the correct macros when using the C18       */
/* compiler.  When using the Hi-Tech PICC18 compiler, refer to the compiler   */
/* manual.pdf in the compiler installation doc directory section on           */
/* 'Configuration Fuses'.  The device header file in the HiTech PICC18        */
/* compiler installation directory contains the available macros to be        */
/* embedded.  The XC8 compiler contains documentation on the configuration    */
/* bit macros within the compiler installation /docs folder in a file called  */
/* pic18_chipinfo.html.                                                       */
/*                                                                            */
/* For additional information about what the hardware configurations mean in  */
/* terms of device operation, refer to the device datasheet.                  */
/*                                                                            */
/* General C18/XC8 syntax for configuration macros:                           */
/* #pragma config <Macro Name>=<Setting>, <Macro Name>=<Setting>, ...         */
/*                                                                            */
/* General HiTech PICC18 syntax:                                              */
/* __CONFIG(n,x);                                                             */
/*                                                                            */
/* n is the config word number and x represents the anded macros from the     */
/* device header file in the PICC18 compiler installation include directory.  */
/*                                                                            */
/* A feature of MPLAB X is the 'Generate Source Code to Output' utility in    */
/* the Configuration Bits window.  Under Window > PIC Memory Views >          */
/* Configuration Bits, a user controllable configuration bits window is       */
/* available to Generate Configuration Bits source code which the user can    */
/* paste into this project.                                                   */
/*                                                                            */
/******************************************************************************/

#if defined(__18F1320__)

#if USE_PLL
#pragma config OSC = HSPLL, FSCM = OFF, IESO = OFF
#else
#pragma config OSC = HS, FSCM = OFF, IESO = OFF
#endif
#pragma config PWRT = ON, BOR  = OFF, BORV = 45
#ifdef __DEBUG
#warning "Debug build!"
#	pragma config WDT = OFF, WDTPS = 512
#else
#	pragma config WDT = ON, WDTPS = 512
#endif
#pragma config MCLRE = ON
#pragma config STVR = OFF, LVP = OFF
#ifdef __DEBUG
#	pragma config CP0 = OFF, CP1 = OFF
#	pragma config CPB = OFF, CPD = OFF
#	pragma config WRT0 = OFF, WRT1 = OFF
#	pragma config WRTC = OFF, WRTB = OFF, WRTD = OFF
#	pragma config EBTR0 = OFF, EBTR1 = OFF
#	pragma config EBTRB = OFF
#else
#	pragma config CP0 = ON, CP1 = ON
#	pragma config CPB = ON, CPD = ON
#	pragma config WRT0 = ON, WRT1 = ON
#	pragma config WRTC = ON, WRTB = ON, WRTD = ON
#	pragma config EBTR0 = ON, EBTR1 = ON
#	pragma config EBTRB = ON
#endif

#endif /* defined(__18F1320__) */

#if  defined(__18F252__)
#if USE_PLL
#pragma config OSC = HSPLL, OSCS = OFF
#else
#pragma config OSC = HS, OSCS = OFF
#endif
#pragma config PWRT = ON, BOR  = OFF, BORV = 45
#pragma config CCP2MUX = 0
#ifdef __DEBUG
#warning "Debug build!"
#	pragma config WDT = OFF, WDTPS = 128
#else
#	pragma config WDT = ON, WDTPS = 128
#endif
#pragma config STVR = OFF, LVP = OFF
#ifdef __DEBUG
#	pragma config CP0 = OFF, CP1 = OFF, CP2 = OFF, CP3 = OFF
#	pragma config CPB = OFF, CPD = OFF
#	pragma config WRT0 = OFF, WRT1 = OFF, WRT2 = OFF, WRT3 = OFF
#	pragma config WRTC = OFF, WRTB = OFF, WRTD = OFF
#	pragma config EBTR0 = OFF, EBTR1 = OFF, EBTR2 = OFF, EBTR3 = OFF
#	pragma config EBTRB = OFF
#else
#	pragma config CP0 = ON, CP1 = ON, CP2 = ON, CP3 = ON
#	pragma config CPB = ON, CPD = ON
#	pragma config WRT0 = ON, WRT1 = ON, WRT2 = ON, WRT3 = ON
#	pragma config WRTC = ON, WRTB = ON, WRTD = ON
#	pragma config EBTR0 = ON, EBTR1 = ON, EBTR2 = ON, EBTR3 = ON
#	pragma config EBTRB = ON
#endif
#endif /* defined(__18F252__) */