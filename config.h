/* 
 * File:   config.h
 * Author: tolyan
 *
 * Created on 9 ??????? 2016 ?., 13:28
 */

#ifndef CONFIG_H
#define	CONFIG_H

#ifdef	__cplusplus
extern "C" {
#endif

#define OK              0
#define ERROR           1

typedef char error_t;

/* Define system operating frequency */
#ifdef F_CPU
#define SYS_FREQ                F_CPU
#define SYS_FREQ_PLL            (F_CPU * 4)
#else
#define SYS_FREQ                10000000UL
#define SYS_FREQ_PLL            (10000000UL * 4)
#endif

#define USE_PLL                 1

/* Microcontroller MIPs (FCY) */  
#define FCY                     SYS_FREQ/4

#define MAX_SOFT_PWMS           3
#define SOFT_PWM_BASE_FREQ      100


#define MAX_BUTTONS             2
#define MAX_VALCODERS           1

#define TASK_QUEUE_SIZE         8
#define MAX_TASKS               3

#define LONG_PUSH_TICKS         7 // max 7

#define SETUP_IDLE_TIME_MS      7000

#define _2BOOL(x)               (x ? TRUE : FALSE)

#define IO_PIN_INPUT            (TRUE)
#define IO_PIN_OUTPUT           (FALSE)

#if USE_PLL
#undef SYS_FREQ
#define SYS_FREQ                SYS_FREQ_PLL
#endif

#ifdef	__cplusplus
}
#endif

#endif	/* CONFIG_H */

