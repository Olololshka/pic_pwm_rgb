opt subtitle "Microchip Technology Omniscient Code Generator (PRO mode) build 59893"

opt pagewidth 120

	opt pm

	processor	18F1320
porta	equ	0F80h
portb	equ	0F81h
portc	equ	0F82h
portd	equ	0F83h
porte	equ	0F84h
lata	equ	0F89h
latb	equ	0F8Ah
latc	equ	0F8Bh
latd	equ	0F8Ch
late	equ	0F8Dh
trisa	equ	0F92h
trisb	equ	0F93h
trisc	equ	0F94h
trisd	equ	0F95h
trise	equ	0F96h
pie1	equ	0F9Dh
pir1	equ	0F9Eh
ipr1	equ	0F9Fh
pie2	equ	0FA0h
pir2	equ	0FA1h
ipr2	equ	0FA2h
t3con	equ	0FB1h
tmr3l	equ	0FB2h
tmr3h	equ	0FB3h
ccp1con	equ	0FBDh
ccpr1l	equ	0FBEh
ccpr1h	equ	0FBFh
adcon1	equ	0FC1h
adcon0	equ	0FC2h
adresl	equ	0FC3h
adresh	equ	0FC4h
sspcon2	equ	0FC5h
sspcon1	equ	0FC6h
sspstat	equ	0FC7h
sspadd	equ	0FC8h
sspbuf	equ	0FC9h
t2con	equ	0FCAh
pr2	equ	0FCBh
tmr2	equ	0FCCh
t1con	equ	0FCDh
tmr1l	equ	0FCEh
tmr1h	equ	0FCFh
rcon	equ	0FD0h
wdtcon	equ	0FD1h
lvdcon	equ	0FD2h
osccon	equ	0FD3h
t0con	equ	0FD5h
tmr0l	equ	0FD6h
tmr0h	equ	0FD7h
status	equ	0FD8h
fsr2	equ	0FD9h
fsr2l	equ	0FD9h
fsr2h	equ	0FDAh
plusw2	equ	0FDBh
preinc2	equ	0FDCh
postdec2	equ	0FDDh
postinc2	equ	0FDEh
indf2	equ	0FDFh
bsr	equ	0FE0h
fsr1	equ	0FE1h
fsr1l	equ	0FE1h
fsr1h	equ	0FE2h
plusw1	equ	0FE3h
preinc1	equ	0FE4h
postdec1	equ	0FE5h
postinc1	equ	0FE6h
indf1	equ	0FE7h
wreg	equ	0FE8h
fsr0	equ	0FE9h
fsr0l	equ	0FE9h
fsr0h	equ	0FEAh
plusw0	equ	0FEBh
preinc0	equ	0FECh
postdec0	equ	0FEDh
postinc0	equ	0FEEh
indf0	equ	0FEFh
intcon3	equ	0FF0h
intcon2	equ	0FF1h
intcon	equ	0FF2h
prod	equ	0FF3h
prodl	equ	0FF3h
prodh	equ	0FF4h
tablat	equ	0FF5h
tblptr	equ	0FF6h
tblptrl	equ	0FF6h
tblptrh	equ	0FF7h
tblptru	equ	0FF8h
pcl	equ	0FF9h
pclat	equ	0FFAh
pclath	equ	0FFAh
pclatu	equ	0FFBh
stkptr	equ	0FFCh
tosl	equ	0FFDh
tosh	equ	0FFEh
tosu	equ	0FFFh
clrc   macro
	bcf	status,0
endm
setc   macro
	bsf	status,0
endm
clrz   macro
	bcf	status,2
endm
setz   macro
	bsf	status,2
endm
skipnz macro
	btfsc	status,2
endm
skipz  macro
	btfss	status,2
endm
skipnc macro
	btfsc	status,0
endm
skipc  macro
	btfss	status,0
endm
pushw macro
	movwf postinc1
endm
pushf macro arg1
	movff arg1, postinc1
endm
popw macro
	movf postdec1,w
	movf indf1,w
endm
popf macro arg1
	movf postdec1,w
	movff indf1,arg1
endm
popfc macro arg1
	movff plusw1,arg1
	decfsz fsr1,f
endm
	global	__ramtop
	global	__accesstop
# 49 "I:\Microchip\xc8\v1.33\include\pic18f1320.h"
PORTA equ 0F80h ;# 
# 242 "I:\Microchip\xc8\v1.33\include\pic18f1320.h"
PORTB equ 0F81h ;# 
# 489 "I:\Microchip\xc8\v1.33\include\pic18f1320.h"
LATA equ 0F89h ;# 
# 616 "I:\Microchip\xc8\v1.33\include\pic18f1320.h"
LATB equ 0F8Ah ;# 
# 748 "I:\Microchip\xc8\v1.33\include\pic18f1320.h"
TRISA equ 0F92h ;# 
# 753 "I:\Microchip\xc8\v1.33\include\pic18f1320.h"
DDRA equ 0F92h ;# 
# 949 "I:\Microchip\xc8\v1.33\include\pic18f1320.h"
TRISB equ 0F93h ;# 
# 954 "I:\Microchip\xc8\v1.33\include\pic18f1320.h"
DDRB equ 0F93h ;# 
# 1170 "I:\Microchip\xc8\v1.33\include\pic18f1320.h"
OSCTUNE equ 0F9Bh ;# 
# 1227 "I:\Microchip\xc8\v1.33\include\pic18f1320.h"
PIE1 equ 0F9Dh ;# 
# 1295 "I:\Microchip\xc8\v1.33\include\pic18f1320.h"
PIR1 equ 0F9Eh ;# 
# 1363 "I:\Microchip\xc8\v1.33\include\pic18f1320.h"
IPR1 equ 0F9Fh ;# 
# 1431 "I:\Microchip\xc8\v1.33\include\pic18f1320.h"
PIE2 equ 0FA0h ;# 
# 1471 "I:\Microchip\xc8\v1.33\include\pic18f1320.h"
PIR2 equ 0FA1h ;# 
# 1511 "I:\Microchip\xc8\v1.33\include\pic18f1320.h"
IPR2 equ 0FA2h ;# 
# 1551 "I:\Microchip\xc8\v1.33\include\pic18f1320.h"
EECON1 equ 0FA6h ;# 
# 1616 "I:\Microchip\xc8\v1.33\include\pic18f1320.h"
EECON2 equ 0FA7h ;# 
# 1622 "I:\Microchip\xc8\v1.33\include\pic18f1320.h"
EEDATA equ 0FA8h ;# 
# 1628 "I:\Microchip\xc8\v1.33\include\pic18f1320.h"
EEADR equ 0FA9h ;# 
# 1634 "I:\Microchip\xc8\v1.33\include\pic18f1320.h"
BAUDCTL equ 0FAAh ;# 
# 1697 "I:\Microchip\xc8\v1.33\include\pic18f1320.h"
RCSTA equ 0FABh ;# 
# 1702 "I:\Microchip\xc8\v1.33\include\pic18f1320.h"
RCSTA1 equ 0FABh ;# 
# 1906 "I:\Microchip\xc8\v1.33\include\pic18f1320.h"
TXSTA equ 0FACh ;# 
# 1911 "I:\Microchip\xc8\v1.33\include\pic18f1320.h"
TXSTA1 equ 0FACh ;# 
# 2203 "I:\Microchip\xc8\v1.33\include\pic18f1320.h"
TXREG equ 0FADh ;# 
# 2208 "I:\Microchip\xc8\v1.33\include\pic18f1320.h"
TXREG1 equ 0FADh ;# 
# 2214 "I:\Microchip\xc8\v1.33\include\pic18f1320.h"
RCREG equ 0FAEh ;# 
# 2219 "I:\Microchip\xc8\v1.33\include\pic18f1320.h"
RCREG1 equ 0FAEh ;# 
# 2225 "I:\Microchip\xc8\v1.33\include\pic18f1320.h"
SPBRG equ 0FAFh ;# 
# 2230 "I:\Microchip\xc8\v1.33\include\pic18f1320.h"
SPBRG1 equ 0FAFh ;# 
# 2236 "I:\Microchip\xc8\v1.33\include\pic18f1320.h"
SPBRGH equ 0FB0h ;# 
# 2242 "I:\Microchip\xc8\v1.33\include\pic18f1320.h"
T3CON equ 0FB1h ;# 
# 2355 "I:\Microchip\xc8\v1.33\include\pic18f1320.h"
TMR3 equ 0FB2h ;# 
# 2361 "I:\Microchip\xc8\v1.33\include\pic18f1320.h"
TMR3L equ 0FB2h ;# 
# 2367 "I:\Microchip\xc8\v1.33\include\pic18f1320.h"
TMR3H equ 0FB3h ;# 
# 2373 "I:\Microchip\xc8\v1.33\include\pic18f1320.h"
ECCPAS equ 0FB6h ;# 
# 2454 "I:\Microchip\xc8\v1.33\include\pic18f1320.h"
PWM1CON equ 0FB7h ;# 
# 2523 "I:\Microchip\xc8\v1.33\include\pic18f1320.h"
CCP1CON equ 0FBDh ;# 
# 2619 "I:\Microchip\xc8\v1.33\include\pic18f1320.h"
CCPR1 equ 0FBEh ;# 
# 2625 "I:\Microchip\xc8\v1.33\include\pic18f1320.h"
CCPR1L equ 0FBEh ;# 
# 2631 "I:\Microchip\xc8\v1.33\include\pic18f1320.h"
CCPR1H equ 0FBFh ;# 
# 2637 "I:\Microchip\xc8\v1.33\include\pic18f1320.h"
ADCON2 equ 0FC0h ;# 
# 2707 "I:\Microchip\xc8\v1.33\include\pic18f1320.h"
ADCON1 equ 0FC1h ;# 
# 2797 "I:\Microchip\xc8\v1.33\include\pic18f1320.h"
ADCON0 equ 0FC2h ;# 
# 2942 "I:\Microchip\xc8\v1.33\include\pic18f1320.h"
ADRES equ 0FC3h ;# 
# 2948 "I:\Microchip\xc8\v1.33\include\pic18f1320.h"
ADRESL equ 0FC3h ;# 
# 2954 "I:\Microchip\xc8\v1.33\include\pic18f1320.h"
ADRESH equ 0FC4h ;# 
# 2960 "I:\Microchip\xc8\v1.33\include\pic18f1320.h"
T2CON equ 0FCAh ;# 
# 3030 "I:\Microchip\xc8\v1.33\include\pic18f1320.h"
PR2 equ 0FCBh ;# 
# 3035 "I:\Microchip\xc8\v1.33\include\pic18f1320.h"
MEMCON equ 0FCBh ;# 
# 3139 "I:\Microchip\xc8\v1.33\include\pic18f1320.h"
TMR2 equ 0FCCh ;# 
# 3145 "I:\Microchip\xc8\v1.33\include\pic18f1320.h"
T1CON equ 0FCDh ;# 
# 3251 "I:\Microchip\xc8\v1.33\include\pic18f1320.h"
TMR1 equ 0FCEh ;# 
# 3257 "I:\Microchip\xc8\v1.33\include\pic18f1320.h"
TMR1L equ 0FCEh ;# 
# 3263 "I:\Microchip\xc8\v1.33\include\pic18f1320.h"
TMR1H equ 0FCFh ;# 
# 3269 "I:\Microchip\xc8\v1.33\include\pic18f1320.h"
RCON equ 0FD0h ;# 
# 3395 "I:\Microchip\xc8\v1.33\include\pic18f1320.h"
WDTCON equ 0FD1h ;# 
# 3422 "I:\Microchip\xc8\v1.33\include\pic18f1320.h"
LVDCON equ 0FD2h ;# 
# 3486 "I:\Microchip\xc8\v1.33\include\pic18f1320.h"
OSCCON equ 0FD3h ;# 
# 3569 "I:\Microchip\xc8\v1.33\include\pic18f1320.h"
T0CON equ 0FD5h ;# 
# 3645 "I:\Microchip\xc8\v1.33\include\pic18f1320.h"
TMR0 equ 0FD6h ;# 
# 3651 "I:\Microchip\xc8\v1.33\include\pic18f1320.h"
TMR0L equ 0FD6h ;# 
# 3657 "I:\Microchip\xc8\v1.33\include\pic18f1320.h"
TMR0H equ 0FD7h ;# 
# 3663 "I:\Microchip\xc8\v1.33\include\pic18f1320.h"
STATUS equ 0FD8h ;# 
# 3741 "I:\Microchip\xc8\v1.33\include\pic18f1320.h"
FSR2 equ 0FD9h ;# 
# 3747 "I:\Microchip\xc8\v1.33\include\pic18f1320.h"
FSR2L equ 0FD9h ;# 
# 3753 "I:\Microchip\xc8\v1.33\include\pic18f1320.h"
FSR2H equ 0FDAh ;# 
# 3759 "I:\Microchip\xc8\v1.33\include\pic18f1320.h"
PLUSW2 equ 0FDBh ;# 
# 3765 "I:\Microchip\xc8\v1.33\include\pic18f1320.h"
PREINC2 equ 0FDCh ;# 
# 3771 "I:\Microchip\xc8\v1.33\include\pic18f1320.h"
POSTDEC2 equ 0FDDh ;# 
# 3777 "I:\Microchip\xc8\v1.33\include\pic18f1320.h"
POSTINC2 equ 0FDEh ;# 
# 3783 "I:\Microchip\xc8\v1.33\include\pic18f1320.h"
INDF2 equ 0FDFh ;# 
# 3789 "I:\Microchip\xc8\v1.33\include\pic18f1320.h"
BSR equ 0FE0h ;# 
# 3795 "I:\Microchip\xc8\v1.33\include\pic18f1320.h"
FSR1 equ 0FE1h ;# 
# 3801 "I:\Microchip\xc8\v1.33\include\pic18f1320.h"
FSR1L equ 0FE1h ;# 
# 3807 "I:\Microchip\xc8\v1.33\include\pic18f1320.h"
FSR1H equ 0FE2h ;# 
# 3813 "I:\Microchip\xc8\v1.33\include\pic18f1320.h"
PLUSW1 equ 0FE3h ;# 
# 3819 "I:\Microchip\xc8\v1.33\include\pic18f1320.h"
PREINC1 equ 0FE4h ;# 
# 3825 "I:\Microchip\xc8\v1.33\include\pic18f1320.h"
POSTDEC1 equ 0FE5h ;# 
# 3831 "I:\Microchip\xc8\v1.33\include\pic18f1320.h"
POSTINC1 equ 0FE6h ;# 
# 3837 "I:\Microchip\xc8\v1.33\include\pic18f1320.h"
INDF1 equ 0FE7h ;# 
# 3843 "I:\Microchip\xc8\v1.33\include\pic18f1320.h"
WREG equ 0FE8h ;# 
# 3854 "I:\Microchip\xc8\v1.33\include\pic18f1320.h"
FSR0 equ 0FE9h ;# 
# 3860 "I:\Microchip\xc8\v1.33\include\pic18f1320.h"
FSR0L equ 0FE9h ;# 
# 3866 "I:\Microchip\xc8\v1.33\include\pic18f1320.h"
FSR0H equ 0FEAh ;# 
# 3872 "I:\Microchip\xc8\v1.33\include\pic18f1320.h"
PLUSW0 equ 0FEBh ;# 
# 3878 "I:\Microchip\xc8\v1.33\include\pic18f1320.h"
PREINC0 equ 0FECh ;# 
# 3884 "I:\Microchip\xc8\v1.33\include\pic18f1320.h"
POSTDEC0 equ 0FEDh ;# 
# 3890 "I:\Microchip\xc8\v1.33\include\pic18f1320.h"
POSTINC0 equ 0FEEh ;# 
# 3896 "I:\Microchip\xc8\v1.33\include\pic18f1320.h"
INDF0 equ 0FEFh ;# 
# 3902 "I:\Microchip\xc8\v1.33\include\pic18f1320.h"
INTCON3 equ 0FF0h ;# 
# 3993 "I:\Microchip\xc8\v1.33\include\pic18f1320.h"
INTCON2 equ 0FF1h ;# 
# 4069 "I:\Microchip\xc8\v1.33\include\pic18f1320.h"
INTCON equ 0FF2h ;# 
# 4219 "I:\Microchip\xc8\v1.33\include\pic18f1320.h"
PROD equ 0FF3h ;# 
# 4225 "I:\Microchip\xc8\v1.33\include\pic18f1320.h"
PRODL equ 0FF3h ;# 
# 4231 "I:\Microchip\xc8\v1.33\include\pic18f1320.h"
PRODH equ 0FF4h ;# 
# 4237 "I:\Microchip\xc8\v1.33\include\pic18f1320.h"
TABLAT equ 0FF5h ;# 
# 4245 "I:\Microchip\xc8\v1.33\include\pic18f1320.h"
TBLPTR equ 0FF6h ;# 
# 4251 "I:\Microchip\xc8\v1.33\include\pic18f1320.h"
TBLPTRL equ 0FF6h ;# 
# 4257 "I:\Microchip\xc8\v1.33\include\pic18f1320.h"
TBLPTRH equ 0FF7h ;# 
# 4263 "I:\Microchip\xc8\v1.33\include\pic18f1320.h"
TBLPTRU equ 0FF8h ;# 
# 4271 "I:\Microchip\xc8\v1.33\include\pic18f1320.h"
PCLAT equ 0FF9h ;# 
# 4278 "I:\Microchip\xc8\v1.33\include\pic18f1320.h"
PC equ 0FF9h ;# 
# 4284 "I:\Microchip\xc8\v1.33\include\pic18f1320.h"
PCL equ 0FF9h ;# 
# 4290 "I:\Microchip\xc8\v1.33\include\pic18f1320.h"
PCLATH equ 0FFAh ;# 
# 4296 "I:\Microchip\xc8\v1.33\include\pic18f1320.h"
PCLATU equ 0FFBh ;# 
# 4302 "I:\Microchip\xc8\v1.33\include\pic18f1320.h"
STKPTR equ 0FFCh ;# 
# 4407 "I:\Microchip\xc8\v1.33\include\pic18f1320.h"
TOS equ 0FFDh ;# 
# 4413 "I:\Microchip\xc8\v1.33\include\pic18f1320.h"
TOSL equ 0FFDh ;# 
# 4419 "I:\Microchip\xc8\v1.33\include\pic18f1320.h"
TOSH equ 0FFEh ;# 
# 4425 "I:\Microchip\xc8\v1.33\include\pic18f1320.h"
TOSU equ 0FFFh ;# 
# 49 "I:\Microchip\xc8\v1.33\include\pic18f1320.h"
PORTA equ 0F80h ;# 
# 242 "I:\Microchip\xc8\v1.33\include\pic18f1320.h"
PORTB equ 0F81h ;# 
# 489 "I:\Microchip\xc8\v1.33\include\pic18f1320.h"
LATA equ 0F89h ;# 
# 616 "I:\Microchip\xc8\v1.33\include\pic18f1320.h"
LATB equ 0F8Ah ;# 
# 748 "I:\Microchip\xc8\v1.33\include\pic18f1320.h"
TRISA equ 0F92h ;# 
# 753 "I:\Microchip\xc8\v1.33\include\pic18f1320.h"
DDRA equ 0F92h ;# 
# 949 "I:\Microchip\xc8\v1.33\include\pic18f1320.h"
TRISB equ 0F93h ;# 
# 954 "I:\Microchip\xc8\v1.33\include\pic18f1320.h"
DDRB equ 0F93h ;# 
# 1170 "I:\Microchip\xc8\v1.33\include\pic18f1320.h"
OSCTUNE equ 0F9Bh ;# 
# 1227 "I:\Microchip\xc8\v1.33\include\pic18f1320.h"
PIE1 equ 0F9Dh ;# 
# 1295 "I:\Microchip\xc8\v1.33\include\pic18f1320.h"
PIR1 equ 0F9Eh ;# 
# 1363 "I:\Microchip\xc8\v1.33\include\pic18f1320.h"
IPR1 equ 0F9Fh ;# 
# 1431 "I:\Microchip\xc8\v1.33\include\pic18f1320.h"
PIE2 equ 0FA0h ;# 
# 1471 "I:\Microchip\xc8\v1.33\include\pic18f1320.h"
PIR2 equ 0FA1h ;# 
# 1511 "I:\Microchip\xc8\v1.33\include\pic18f1320.h"
IPR2 equ 0FA2h ;# 
# 1551 "I:\Microchip\xc8\v1.33\include\pic18f1320.h"
EECON1 equ 0FA6h ;# 
# 1616 "I:\Microchip\xc8\v1.33\include\pic18f1320.h"
EECON2 equ 0FA7h ;# 
# 1622 "I:\Microchip\xc8\v1.33\include\pic18f1320.h"
EEDATA equ 0FA8h ;# 
# 1628 "I:\Microchip\xc8\v1.33\include\pic18f1320.h"
EEADR equ 0FA9h ;# 
# 1634 "I:\Microchip\xc8\v1.33\include\pic18f1320.h"
BAUDCTL equ 0FAAh ;# 
# 1697 "I:\Microchip\xc8\v1.33\include\pic18f1320.h"
RCSTA equ 0FABh ;# 
# 1702 "I:\Microchip\xc8\v1.33\include\pic18f1320.h"
RCSTA1 equ 0FABh ;# 
# 1906 "I:\Microchip\xc8\v1.33\include\pic18f1320.h"
TXSTA equ 0FACh ;# 
# 1911 "I:\Microchip\xc8\v1.33\include\pic18f1320.h"
TXSTA1 equ 0FACh ;# 
# 2203 "I:\Microchip\xc8\v1.33\include\pic18f1320.h"
TXREG equ 0FADh ;# 
# 2208 "I:\Microchip\xc8\v1.33\include\pic18f1320.h"
TXREG1 equ 0FADh ;# 
# 2214 "I:\Microchip\xc8\v1.33\include\pic18f1320.h"
RCREG equ 0FAEh ;# 
# 2219 "I:\Microchip\xc8\v1.33\include\pic18f1320.h"
RCREG1 equ 0FAEh ;# 
# 2225 "I:\Microchip\xc8\v1.33\include\pic18f1320.h"
SPBRG equ 0FAFh ;# 
# 2230 "I:\Microchip\xc8\v1.33\include\pic18f1320.h"
SPBRG1 equ 0FAFh ;# 
# 2236 "I:\Microchip\xc8\v1.33\include\pic18f1320.h"
SPBRGH equ 0FB0h ;# 
# 2242 "I:\Microchip\xc8\v1.33\include\pic18f1320.h"
T3CON equ 0FB1h ;# 
# 2355 "I:\Microchip\xc8\v1.33\include\pic18f1320.h"
TMR3 equ 0FB2h ;# 
# 2361 "I:\Microchip\xc8\v1.33\include\pic18f1320.h"
TMR3L equ 0FB2h ;# 
# 2367 "I:\Microchip\xc8\v1.33\include\pic18f1320.h"
TMR3H equ 0FB3h ;# 
# 2373 "I:\Microchip\xc8\v1.33\include\pic18f1320.h"
ECCPAS equ 0FB6h ;# 
# 2454 "I:\Microchip\xc8\v1.33\include\pic18f1320.h"
PWM1CON equ 0FB7h ;# 
# 2523 "I:\Microchip\xc8\v1.33\include\pic18f1320.h"
CCP1CON equ 0FBDh ;# 
# 2619 "I:\Microchip\xc8\v1.33\include\pic18f1320.h"
CCPR1 equ 0FBEh ;# 
# 2625 "I:\Microchip\xc8\v1.33\include\pic18f1320.h"
CCPR1L equ 0FBEh ;# 
# 2631 "I:\Microchip\xc8\v1.33\include\pic18f1320.h"
CCPR1H equ 0FBFh ;# 
# 2637 "I:\Microchip\xc8\v1.33\include\pic18f1320.h"
ADCON2 equ 0FC0h ;# 
# 2707 "I:\Microchip\xc8\v1.33\include\pic18f1320.h"
ADCON1 equ 0FC1h ;# 
# 2797 "I:\Microchip\xc8\v1.33\include\pic18f1320.h"
ADCON0 equ 0FC2h ;# 
# 2942 "I:\Microchip\xc8\v1.33\include\pic18f1320.h"
ADRES equ 0FC3h ;# 
# 2948 "I:\Microchip\xc8\v1.33\include\pic18f1320.h"
ADRESL equ 0FC3h ;# 
# 2954 "I:\Microchip\xc8\v1.33\include\pic18f1320.h"
ADRESH equ 0FC4h ;# 
# 2960 "I:\Microchip\xc8\v1.33\include\pic18f1320.h"
T2CON equ 0FCAh ;# 
# 3030 "I:\Microchip\xc8\v1.33\include\pic18f1320.h"
PR2 equ 0FCBh ;# 
# 3035 "I:\Microchip\xc8\v1.33\include\pic18f1320.h"
MEMCON equ 0FCBh ;# 
# 3139 "I:\Microchip\xc8\v1.33\include\pic18f1320.h"
TMR2 equ 0FCCh ;# 
# 3145 "I:\Microchip\xc8\v1.33\include\pic18f1320.h"
T1CON equ 0FCDh ;# 
# 3251 "I:\Microchip\xc8\v1.33\include\pic18f1320.h"
TMR1 equ 0FCEh ;# 
# 3257 "I:\Microchip\xc8\v1.33\include\pic18f1320.h"
TMR1L equ 0FCEh ;# 
# 3263 "I:\Microchip\xc8\v1.33\include\pic18f1320.h"
TMR1H equ 0FCFh ;# 
# 3269 "I:\Microchip\xc8\v1.33\include\pic18f1320.h"
RCON equ 0FD0h ;# 
# 3395 "I:\Microchip\xc8\v1.33\include\pic18f1320.h"
WDTCON equ 0FD1h ;# 
# 3422 "I:\Microchip\xc8\v1.33\include\pic18f1320.h"
LVDCON equ 0FD2h ;# 
# 3486 "I:\Microchip\xc8\v1.33\include\pic18f1320.h"
OSCCON equ 0FD3h ;# 
# 3569 "I:\Microchip\xc8\v1.33\include\pic18f1320.h"
T0CON equ 0FD5h ;# 
# 3645 "I:\Microchip\xc8\v1.33\include\pic18f1320.h"
TMR0 equ 0FD6h ;# 
# 3651 "I:\Microchip\xc8\v1.33\include\pic18f1320.h"
TMR0L equ 0FD6h ;# 
# 3657 "I:\Microchip\xc8\v1.33\include\pic18f1320.h"
TMR0H equ 0FD7h ;# 
# 3663 "I:\Microchip\xc8\v1.33\include\pic18f1320.h"
STATUS equ 0FD8h ;# 
# 3741 "I:\Microchip\xc8\v1.33\include\pic18f1320.h"
FSR2 equ 0FD9h ;# 
# 3747 "I:\Microchip\xc8\v1.33\include\pic18f1320.h"
FSR2L equ 0FD9h ;# 
# 3753 "I:\Microchip\xc8\v1.33\include\pic18f1320.h"
FSR2H equ 0FDAh ;# 
# 3759 "I:\Microchip\xc8\v1.33\include\pic18f1320.h"
PLUSW2 equ 0FDBh ;# 
# 3765 "I:\Microchip\xc8\v1.33\include\pic18f1320.h"
PREINC2 equ 0FDCh ;# 
# 3771 "I:\Microchip\xc8\v1.33\include\pic18f1320.h"
POSTDEC2 equ 0FDDh ;# 
# 3777 "I:\Microchip\xc8\v1.33\include\pic18f1320.h"
POSTINC2 equ 0FDEh ;# 
# 3783 "I:\Microchip\xc8\v1.33\include\pic18f1320.h"
INDF2 equ 0FDFh ;# 
# 3789 "I:\Microchip\xc8\v1.33\include\pic18f1320.h"
BSR equ 0FE0h ;# 
# 3795 "I:\Microchip\xc8\v1.33\include\pic18f1320.h"
FSR1 equ 0FE1h ;# 
# 3801 "I:\Microchip\xc8\v1.33\include\pic18f1320.h"
FSR1L equ 0FE1h ;# 
# 3807 "I:\Microchip\xc8\v1.33\include\pic18f1320.h"
FSR1H equ 0FE2h ;# 
# 3813 "I:\Microchip\xc8\v1.33\include\pic18f1320.h"
PLUSW1 equ 0FE3h ;# 
# 3819 "I:\Microchip\xc8\v1.33\include\pic18f1320.h"
PREINC1 equ 0FE4h ;# 
# 3825 "I:\Microchip\xc8\v1.33\include\pic18f1320.h"
POSTDEC1 equ 0FE5h ;# 
# 3831 "I:\Microchip\xc8\v1.33\include\pic18f1320.h"
POSTINC1 equ 0FE6h ;# 
# 3837 "I:\Microchip\xc8\v1.33\include\pic18f1320.h"
INDF1 equ 0FE7h ;# 
# 3843 "I:\Microchip\xc8\v1.33\include\pic18f1320.h"
WREG equ 0FE8h ;# 
# 3854 "I:\Microchip\xc8\v1.33\include\pic18f1320.h"
FSR0 equ 0FE9h ;# 
# 3860 "I:\Microchip\xc8\v1.33\include\pic18f1320.h"
FSR0L equ 0FE9h ;# 
# 3866 "I:\Microchip\xc8\v1.33\include\pic18f1320.h"
FSR0H equ 0FEAh ;# 
# 3872 "I:\Microchip\xc8\v1.33\include\pic18f1320.h"
PLUSW0 equ 0FEBh ;# 
# 3878 "I:\Microchip\xc8\v1.33\include\pic18f1320.h"
PREINC0 equ 0FECh ;# 
# 3884 "I:\Microchip\xc8\v1.33\include\pic18f1320.h"
POSTDEC0 equ 0FEDh ;# 
# 3890 "I:\Microchip\xc8\v1.33\include\pic18f1320.h"
POSTINC0 equ 0FEEh ;# 
# 3896 "I:\Microchip\xc8\v1.33\include\pic18f1320.h"
INDF0 equ 0FEFh ;# 
# 3902 "I:\Microchip\xc8\v1.33\include\pic18f1320.h"
INTCON3 equ 0FF0h ;# 
# 3993 "I:\Microchip\xc8\v1.33\include\pic18f1320.h"
INTCON2 equ 0FF1h ;# 
# 4069 "I:\Microchip\xc8\v1.33\include\pic18f1320.h"
INTCON equ 0FF2h ;# 
# 4219 "I:\Microchip\xc8\v1.33\include\pic18f1320.h"
PROD equ 0FF3h ;# 
# 4225 "I:\Microchip\xc8\v1.33\include\pic18f1320.h"
PRODL equ 0FF3h ;# 
# 4231 "I:\Microchip\xc8\v1.33\include\pic18f1320.h"
PRODH equ 0FF4h ;# 
# 4237 "I:\Microchip\xc8\v1.33\include\pic18f1320.h"
TABLAT equ 0FF5h ;# 
# 4245 "I:\Microchip\xc8\v1.33\include\pic18f1320.h"
TBLPTR equ 0FF6h ;# 
# 4251 "I:\Microchip\xc8\v1.33\include\pic18f1320.h"
TBLPTRL equ 0FF6h ;# 
# 4257 "I:\Microchip\xc8\v1.33\include\pic18f1320.h"
TBLPTRH equ 0FF7h ;# 
# 4263 "I:\Microchip\xc8\v1.33\include\pic18f1320.h"
TBLPTRU equ 0FF8h ;# 
# 4271 "I:\Microchip\xc8\v1.33\include\pic18f1320.h"
PCLAT equ 0FF9h ;# 
# 4278 "I:\Microchip\xc8\v1.33\include\pic18f1320.h"
PC equ 0FF9h ;# 
# 4284 "I:\Microchip\xc8\v1.33\include\pic18f1320.h"
PCL equ 0FF9h ;# 
# 4290 "I:\Microchip\xc8\v1.33\include\pic18f1320.h"
PCLATH equ 0FFAh ;# 
# 4296 "I:\Microchip\xc8\v1.33\include\pic18f1320.h"
PCLATU equ 0FFBh ;# 
# 4302 "I:\Microchip\xc8\v1.33\include\pic18f1320.h"
STKPTR equ 0FFCh ;# 
# 4407 "I:\Microchip\xc8\v1.33\include\pic18f1320.h"
TOS equ 0FFDh ;# 
# 4413 "I:\Microchip\xc8\v1.33\include\pic18f1320.h"
TOSL equ 0FFDh ;# 
# 4419 "I:\Microchip\xc8\v1.33\include\pic18f1320.h"
TOSH equ 0FFEh ;# 
# 4425 "I:\Microchip\xc8\v1.33\include\pic18f1320.h"
TOSU equ 0FFFh ;# 
# 49 "I:\Microchip\xc8\v1.33\include\pic18f1320.h"
PORTA equ 0F80h ;# 
# 242 "I:\Microchip\xc8\v1.33\include\pic18f1320.h"
PORTB equ 0F81h ;# 
# 489 "I:\Microchip\xc8\v1.33\include\pic18f1320.h"
LATA equ 0F89h ;# 
# 616 "I:\Microchip\xc8\v1.33\include\pic18f1320.h"
LATB equ 0F8Ah ;# 
# 748 "I:\Microchip\xc8\v1.33\include\pic18f1320.h"
TRISA equ 0F92h ;# 
# 753 "I:\Microchip\xc8\v1.33\include\pic18f1320.h"
DDRA equ 0F92h ;# 
# 949 "I:\Microchip\xc8\v1.33\include\pic18f1320.h"
TRISB equ 0F93h ;# 
# 954 "I:\Microchip\xc8\v1.33\include\pic18f1320.h"
DDRB equ 0F93h ;# 
# 1170 "I:\Microchip\xc8\v1.33\include\pic18f1320.h"
OSCTUNE equ 0F9Bh ;# 
# 1227 "I:\Microchip\xc8\v1.33\include\pic18f1320.h"
PIE1 equ 0F9Dh ;# 
# 1295 "I:\Microchip\xc8\v1.33\include\pic18f1320.h"
PIR1 equ 0F9Eh ;# 
# 1363 "I:\Microchip\xc8\v1.33\include\pic18f1320.h"
IPR1 equ 0F9Fh ;# 
# 1431 "I:\Microchip\xc8\v1.33\include\pic18f1320.h"
PIE2 equ 0FA0h ;# 
# 1471 "I:\Microchip\xc8\v1.33\include\pic18f1320.h"
PIR2 equ 0FA1h ;# 
# 1511 "I:\Microchip\xc8\v1.33\include\pic18f1320.h"
IPR2 equ 0FA2h ;# 
# 1551 "I:\Microchip\xc8\v1.33\include\pic18f1320.h"
EECON1 equ 0FA6h ;# 
# 1616 "I:\Microchip\xc8\v1.33\include\pic18f1320.h"
EECON2 equ 0FA7h ;# 
# 1622 "I:\Microchip\xc8\v1.33\include\pic18f1320.h"
EEDATA equ 0FA8h ;# 
# 1628 "I:\Microchip\xc8\v1.33\include\pic18f1320.h"
EEADR equ 0FA9h ;# 
# 1634 "I:\Microchip\xc8\v1.33\include\pic18f1320.h"
BAUDCTL equ 0FAAh ;# 
# 1697 "I:\Microchip\xc8\v1.33\include\pic18f1320.h"
RCSTA equ 0FABh ;# 
# 1702 "I:\Microchip\xc8\v1.33\include\pic18f1320.h"
RCSTA1 equ 0FABh ;# 
# 1906 "I:\Microchip\xc8\v1.33\include\pic18f1320.h"
TXSTA equ 0FACh ;# 
# 1911 "I:\Microchip\xc8\v1.33\include\pic18f1320.h"
TXSTA1 equ 0FACh ;# 
# 2203 "I:\Microchip\xc8\v1.33\include\pic18f1320.h"
TXREG equ 0FADh ;# 
# 2208 "I:\Microchip\xc8\v1.33\include\pic18f1320.h"
TXREG1 equ 0FADh ;# 
# 2214 "I:\Microchip\xc8\v1.33\include\pic18f1320.h"
RCREG equ 0FAEh ;# 
# 2219 "I:\Microchip\xc8\v1.33\include\pic18f1320.h"
RCREG1 equ 0FAEh ;# 
# 2225 "I:\Microchip\xc8\v1.33\include\pic18f1320.h"
SPBRG equ 0FAFh ;# 
# 2230 "I:\Microchip\xc8\v1.33\include\pic18f1320.h"
SPBRG1 equ 0FAFh ;# 
# 2236 "I:\Microchip\xc8\v1.33\include\pic18f1320.h"
SPBRGH equ 0FB0h ;# 
# 2242 "I:\Microchip\xc8\v1.33\include\pic18f1320.h"
T3CON equ 0FB1h ;# 
# 2355 "I:\Microchip\xc8\v1.33\include\pic18f1320.h"
TMR3 equ 0FB2h ;# 
# 2361 "I:\Microchip\xc8\v1.33\include\pic18f1320.h"
TMR3L equ 0FB2h ;# 
# 2367 "I:\Microchip\xc8\v1.33\include\pic18f1320.h"
TMR3H equ 0FB3h ;# 
# 2373 "I:\Microchip\xc8\v1.33\include\pic18f1320.h"
ECCPAS equ 0FB6h ;# 
# 2454 "I:\Microchip\xc8\v1.33\include\pic18f1320.h"
PWM1CON equ 0FB7h ;# 
# 2523 "I:\Microchip\xc8\v1.33\include\pic18f1320.h"
CCP1CON equ 0FBDh ;# 
# 2619 "I:\Microchip\xc8\v1.33\include\pic18f1320.h"
CCPR1 equ 0FBEh ;# 
# 2625 "I:\Microchip\xc8\v1.33\include\pic18f1320.h"
CCPR1L equ 0FBEh ;# 
# 2631 "I:\Microchip\xc8\v1.33\include\pic18f1320.h"
CCPR1H equ 0FBFh ;# 
# 2637 "I:\Microchip\xc8\v1.33\include\pic18f1320.h"
ADCON2 equ 0FC0h ;# 
# 2707 "I:\Microchip\xc8\v1.33\include\pic18f1320.h"
ADCON1 equ 0FC1h ;# 
# 2797 "I:\Microchip\xc8\v1.33\include\pic18f1320.h"
ADCON0 equ 0FC2h ;# 
# 2942 "I:\Microchip\xc8\v1.33\include\pic18f1320.h"
ADRES equ 0FC3h ;# 
# 2948 "I:\Microchip\xc8\v1.33\include\pic18f1320.h"
ADRESL equ 0FC3h ;# 
# 2954 "I:\Microchip\xc8\v1.33\include\pic18f1320.h"
ADRESH equ 0FC4h ;# 
# 2960 "I:\Microchip\xc8\v1.33\include\pic18f1320.h"
T2CON equ 0FCAh ;# 
# 3030 "I:\Microchip\xc8\v1.33\include\pic18f1320.h"
PR2 equ 0FCBh ;# 
# 3035 "I:\Microchip\xc8\v1.33\include\pic18f1320.h"
MEMCON equ 0FCBh ;# 
# 3139 "I:\Microchip\xc8\v1.33\include\pic18f1320.h"
TMR2 equ 0FCCh ;# 
# 3145 "I:\Microchip\xc8\v1.33\include\pic18f1320.h"
T1CON equ 0FCDh ;# 
# 3251 "I:\Microchip\xc8\v1.33\include\pic18f1320.h"
TMR1 equ 0FCEh ;# 
# 3257 "I:\Microchip\xc8\v1.33\include\pic18f1320.h"
TMR1L equ 0FCEh ;# 
# 3263 "I:\Microchip\xc8\v1.33\include\pic18f1320.h"
TMR1H equ 0FCFh ;# 
# 3269 "I:\Microchip\xc8\v1.33\include\pic18f1320.h"
RCON equ 0FD0h ;# 
# 3395 "I:\Microchip\xc8\v1.33\include\pic18f1320.h"
WDTCON equ 0FD1h ;# 
# 3422 "I:\Microchip\xc8\v1.33\include\pic18f1320.h"
LVDCON equ 0FD2h ;# 
# 3486 "I:\Microchip\xc8\v1.33\include\pic18f1320.h"
OSCCON equ 0FD3h ;# 
# 3569 "I:\Microchip\xc8\v1.33\include\pic18f1320.h"
T0CON equ 0FD5h ;# 
# 3645 "I:\Microchip\xc8\v1.33\include\pic18f1320.h"
TMR0 equ 0FD6h ;# 
# 3651 "I:\Microchip\xc8\v1.33\include\pic18f1320.h"
TMR0L equ 0FD6h ;# 
# 3657 "I:\Microchip\xc8\v1.33\include\pic18f1320.h"
TMR0H equ 0FD7h ;# 
# 3663 "I:\Microchip\xc8\v1.33\include\pic18f1320.h"
STATUS equ 0FD8h ;# 
# 3741 "I:\Microchip\xc8\v1.33\include\pic18f1320.h"
FSR2 equ 0FD9h ;# 
# 3747 "I:\Microchip\xc8\v1.33\include\pic18f1320.h"
FSR2L equ 0FD9h ;# 
# 3753 "I:\Microchip\xc8\v1.33\include\pic18f1320.h"
FSR2H equ 0FDAh ;# 
# 3759 "I:\Microchip\xc8\v1.33\include\pic18f1320.h"
PLUSW2 equ 0FDBh ;# 
# 3765 "I:\Microchip\xc8\v1.33\include\pic18f1320.h"
PREINC2 equ 0FDCh ;# 
# 3771 "I:\Microchip\xc8\v1.33\include\pic18f1320.h"
POSTDEC2 equ 0FDDh ;# 
# 3777 "I:\Microchip\xc8\v1.33\include\pic18f1320.h"
POSTINC2 equ 0FDEh ;# 
# 3783 "I:\Microchip\xc8\v1.33\include\pic18f1320.h"
INDF2 equ 0FDFh ;# 
# 3789 "I:\Microchip\xc8\v1.33\include\pic18f1320.h"
BSR equ 0FE0h ;# 
# 3795 "I:\Microchip\xc8\v1.33\include\pic18f1320.h"
FSR1 equ 0FE1h ;# 
# 3801 "I:\Microchip\xc8\v1.33\include\pic18f1320.h"
FSR1L equ 0FE1h ;# 
# 3807 "I:\Microchip\xc8\v1.33\include\pic18f1320.h"
FSR1H equ 0FE2h ;# 
# 3813 "I:\Microchip\xc8\v1.33\include\pic18f1320.h"
PLUSW1 equ 0FE3h ;# 
# 3819 "I:\Microchip\xc8\v1.33\include\pic18f1320.h"
PREINC1 equ 0FE4h ;# 
# 3825 "I:\Microchip\xc8\v1.33\include\pic18f1320.h"
POSTDEC1 equ 0FE5h ;# 
# 3831 "I:\Microchip\xc8\v1.33\include\pic18f1320.h"
POSTINC1 equ 0FE6h ;# 
# 3837 "I:\Microchip\xc8\v1.33\include\pic18f1320.h"
INDF1 equ 0FE7h ;# 
# 3843 "I:\Microchip\xc8\v1.33\include\pic18f1320.h"
WREG equ 0FE8h ;# 
# 3854 "I:\Microchip\xc8\v1.33\include\pic18f1320.h"
FSR0 equ 0FE9h ;# 
# 3860 "I:\Microchip\xc8\v1.33\include\pic18f1320.h"
FSR0L equ 0FE9h ;# 
# 3866 "I:\Microchip\xc8\v1.33\include\pic18f1320.h"
FSR0H equ 0FEAh ;# 
# 3872 "I:\Microchip\xc8\v1.33\include\pic18f1320.h"
PLUSW0 equ 0FEBh ;# 
# 3878 "I:\Microchip\xc8\v1.33\include\pic18f1320.h"
PREINC0 equ 0FECh ;# 
# 3884 "I:\Microchip\xc8\v1.33\include\pic18f1320.h"
POSTDEC0 equ 0FEDh ;# 
# 3890 "I:\Microchip\xc8\v1.33\include\pic18f1320.h"
POSTINC0 equ 0FEEh ;# 
# 3896 "I:\Microchip\xc8\v1.33\include\pic18f1320.h"
INDF0 equ 0FEFh ;# 
# 3902 "I:\Microchip\xc8\v1.33\include\pic18f1320.h"
INTCON3 equ 0FF0h ;# 
# 3993 "I:\Microchip\xc8\v1.33\include\pic18f1320.h"
INTCON2 equ 0FF1h ;# 
# 4069 "I:\Microchip\xc8\v1.33\include\pic18f1320.h"
INTCON equ 0FF2h ;# 
# 4219 "I:\Microchip\xc8\v1.33\include\pic18f1320.h"
PROD equ 0FF3h ;# 
# 4225 "I:\Microchip\xc8\v1.33\include\pic18f1320.h"
PRODL equ 0FF3h ;# 
# 4231 "I:\Microchip\xc8\v1.33\include\pic18f1320.h"
PRODH equ 0FF4h ;# 
# 4237 "I:\Microchip\xc8\v1.33\include\pic18f1320.h"
TABLAT equ 0FF5h ;# 
# 4245 "I:\Microchip\xc8\v1.33\include\pic18f1320.h"
TBLPTR equ 0FF6h ;# 
# 4251 "I:\Microchip\xc8\v1.33\include\pic18f1320.h"
TBLPTRL equ 0FF6h ;# 
# 4257 "I:\Microchip\xc8\v1.33\include\pic18f1320.h"
TBLPTRH equ 0FF7h ;# 
# 4263 "I:\Microchip\xc8\v1.33\include\pic18f1320.h"
TBLPTRU equ 0FF8h ;# 
# 4271 "I:\Microchip\xc8\v1.33\include\pic18f1320.h"
PCLAT equ 0FF9h ;# 
# 4278 "I:\Microchip\xc8\v1.33\include\pic18f1320.h"
PC equ 0FF9h ;# 
# 4284 "I:\Microchip\xc8\v1.33\include\pic18f1320.h"
PCL equ 0FF9h ;# 
# 4290 "I:\Microchip\xc8\v1.33\include\pic18f1320.h"
PCLATH equ 0FFAh ;# 
# 4296 "I:\Microchip\xc8\v1.33\include\pic18f1320.h"
PCLATU equ 0FFBh ;# 
# 4302 "I:\Microchip\xc8\v1.33\include\pic18f1320.h"
STKPTR equ 0FFCh ;# 
# 4407 "I:\Microchip\xc8\v1.33\include\pic18f1320.h"
TOS equ 0FFDh ;# 
# 4413 "I:\Microchip\xc8\v1.33\include\pic18f1320.h"
TOSL equ 0FFDh ;# 
# 4419 "I:\Microchip\xc8\v1.33\include\pic18f1320.h"
TOSH equ 0FFEh ;# 
# 4425 "I:\Microchip\xc8\v1.33\include\pic18f1320.h"
TOSU equ 0FFFh ;# 
# 49 "I:\Microchip\xc8\v1.33\include\pic18f1320.h"
PORTA equ 0F80h ;# 
# 242 "I:\Microchip\xc8\v1.33\include\pic18f1320.h"
PORTB equ 0F81h ;# 
# 489 "I:\Microchip\xc8\v1.33\include\pic18f1320.h"
LATA equ 0F89h ;# 
# 616 "I:\Microchip\xc8\v1.33\include\pic18f1320.h"
LATB equ 0F8Ah ;# 
# 748 "I:\Microchip\xc8\v1.33\include\pic18f1320.h"
TRISA equ 0F92h ;# 
# 753 "I:\Microchip\xc8\v1.33\include\pic18f1320.h"
DDRA equ 0F92h ;# 
# 949 "I:\Microchip\xc8\v1.33\include\pic18f1320.h"
TRISB equ 0F93h ;# 
# 954 "I:\Microchip\xc8\v1.33\include\pic18f1320.h"
DDRB equ 0F93h ;# 
# 1170 "I:\Microchip\xc8\v1.33\include\pic18f1320.h"
OSCTUNE equ 0F9Bh ;# 
# 1227 "I:\Microchip\xc8\v1.33\include\pic18f1320.h"
PIE1 equ 0F9Dh ;# 
# 1295 "I:\Microchip\xc8\v1.33\include\pic18f1320.h"
PIR1 equ 0F9Eh ;# 
# 1363 "I:\Microchip\xc8\v1.33\include\pic18f1320.h"
IPR1 equ 0F9Fh ;# 
# 1431 "I:\Microchip\xc8\v1.33\include\pic18f1320.h"
PIE2 equ 0FA0h ;# 
# 1471 "I:\Microchip\xc8\v1.33\include\pic18f1320.h"
PIR2 equ 0FA1h ;# 
# 1511 "I:\Microchip\xc8\v1.33\include\pic18f1320.h"
IPR2 equ 0FA2h ;# 
# 1551 "I:\Microchip\xc8\v1.33\include\pic18f1320.h"
EECON1 equ 0FA6h ;# 
# 1616 "I:\Microchip\xc8\v1.33\include\pic18f1320.h"
EECON2 equ 0FA7h ;# 
# 1622 "I:\Microchip\xc8\v1.33\include\pic18f1320.h"
EEDATA equ 0FA8h ;# 
# 1628 "I:\Microchip\xc8\v1.33\include\pic18f1320.h"
EEADR equ 0FA9h ;# 
# 1634 "I:\Microchip\xc8\v1.33\include\pic18f1320.h"
BAUDCTL equ 0FAAh ;# 
# 1697 "I:\Microchip\xc8\v1.33\include\pic18f1320.h"
RCSTA equ 0FABh ;# 
# 1702 "I:\Microchip\xc8\v1.33\include\pic18f1320.h"
RCSTA1 equ 0FABh ;# 
# 1906 "I:\Microchip\xc8\v1.33\include\pic18f1320.h"
TXSTA equ 0FACh ;# 
# 1911 "I:\Microchip\xc8\v1.33\include\pic18f1320.h"
TXSTA1 equ 0FACh ;# 
# 2203 "I:\Microchip\xc8\v1.33\include\pic18f1320.h"
TXREG equ 0FADh ;# 
# 2208 "I:\Microchip\xc8\v1.33\include\pic18f1320.h"
TXREG1 equ 0FADh ;# 
# 2214 "I:\Microchip\xc8\v1.33\include\pic18f1320.h"
RCREG equ 0FAEh ;# 
# 2219 "I:\Microchip\xc8\v1.33\include\pic18f1320.h"
RCREG1 equ 0FAEh ;# 
# 2225 "I:\Microchip\xc8\v1.33\include\pic18f1320.h"
SPBRG equ 0FAFh ;# 
# 2230 "I:\Microchip\xc8\v1.33\include\pic18f1320.h"
SPBRG1 equ 0FAFh ;# 
# 2236 "I:\Microchip\xc8\v1.33\include\pic18f1320.h"
SPBRGH equ 0FB0h ;# 
# 2242 "I:\Microchip\xc8\v1.33\include\pic18f1320.h"
T3CON equ 0FB1h ;# 
# 2355 "I:\Microchip\xc8\v1.33\include\pic18f1320.h"
TMR3 equ 0FB2h ;# 
# 2361 "I:\Microchip\xc8\v1.33\include\pic18f1320.h"
TMR3L equ 0FB2h ;# 
# 2367 "I:\Microchip\xc8\v1.33\include\pic18f1320.h"
TMR3H equ 0FB3h ;# 
# 2373 "I:\Microchip\xc8\v1.33\include\pic18f1320.h"
ECCPAS equ 0FB6h ;# 
# 2454 "I:\Microchip\xc8\v1.33\include\pic18f1320.h"
PWM1CON equ 0FB7h ;# 
# 2523 "I:\Microchip\xc8\v1.33\include\pic18f1320.h"
CCP1CON equ 0FBDh ;# 
# 2619 "I:\Microchip\xc8\v1.33\include\pic18f1320.h"
CCPR1 equ 0FBEh ;# 
# 2625 "I:\Microchip\xc8\v1.33\include\pic18f1320.h"
CCPR1L equ 0FBEh ;# 
# 2631 "I:\Microchip\xc8\v1.33\include\pic18f1320.h"
CCPR1H equ 0FBFh ;# 
# 2637 "I:\Microchip\xc8\v1.33\include\pic18f1320.h"
ADCON2 equ 0FC0h ;# 
# 2707 "I:\Microchip\xc8\v1.33\include\pic18f1320.h"
ADCON1 equ 0FC1h ;# 
# 2797 "I:\Microchip\xc8\v1.33\include\pic18f1320.h"
ADCON0 equ 0FC2h ;# 
# 2942 "I:\Microchip\xc8\v1.33\include\pic18f1320.h"
ADRES equ 0FC3h ;# 
# 2948 "I:\Microchip\xc8\v1.33\include\pic18f1320.h"
ADRESL equ 0FC3h ;# 
# 2954 "I:\Microchip\xc8\v1.33\include\pic18f1320.h"
ADRESH equ 0FC4h ;# 
# 2960 "I:\Microchip\xc8\v1.33\include\pic18f1320.h"
T2CON equ 0FCAh ;# 
# 3030 "I:\Microchip\xc8\v1.33\include\pic18f1320.h"
PR2 equ 0FCBh ;# 
# 3035 "I:\Microchip\xc8\v1.33\include\pic18f1320.h"
MEMCON equ 0FCBh ;# 
# 3139 "I:\Microchip\xc8\v1.33\include\pic18f1320.h"
TMR2 equ 0FCCh ;# 
# 3145 "I:\Microchip\xc8\v1.33\include\pic18f1320.h"
T1CON equ 0FCDh ;# 
# 3251 "I:\Microchip\xc8\v1.33\include\pic18f1320.h"
TMR1 equ 0FCEh ;# 
# 3257 "I:\Microchip\xc8\v1.33\include\pic18f1320.h"
TMR1L equ 0FCEh ;# 
# 3263 "I:\Microchip\xc8\v1.33\include\pic18f1320.h"
TMR1H equ 0FCFh ;# 
# 3269 "I:\Microchip\xc8\v1.33\include\pic18f1320.h"
RCON equ 0FD0h ;# 
# 3395 "I:\Microchip\xc8\v1.33\include\pic18f1320.h"
WDTCON equ 0FD1h ;# 
# 3422 "I:\Microchip\xc8\v1.33\include\pic18f1320.h"
LVDCON equ 0FD2h ;# 
# 3486 "I:\Microchip\xc8\v1.33\include\pic18f1320.h"
OSCCON equ 0FD3h ;# 
# 3569 "I:\Microchip\xc8\v1.33\include\pic18f1320.h"
T0CON equ 0FD5h ;# 
# 3645 "I:\Microchip\xc8\v1.33\include\pic18f1320.h"
TMR0 equ 0FD6h ;# 
# 3651 "I:\Microchip\xc8\v1.33\include\pic18f1320.h"
TMR0L equ 0FD6h ;# 
# 3657 "I:\Microchip\xc8\v1.33\include\pic18f1320.h"
TMR0H equ 0FD7h ;# 
# 3663 "I:\Microchip\xc8\v1.33\include\pic18f1320.h"
STATUS equ 0FD8h ;# 
# 3741 "I:\Microchip\xc8\v1.33\include\pic18f1320.h"
FSR2 equ 0FD9h ;# 
# 3747 "I:\Microchip\xc8\v1.33\include\pic18f1320.h"
FSR2L equ 0FD9h ;# 
# 3753 "I:\Microchip\xc8\v1.33\include\pic18f1320.h"
FSR2H equ 0FDAh ;# 
# 3759 "I:\Microchip\xc8\v1.33\include\pic18f1320.h"
PLUSW2 equ 0FDBh ;# 
# 3765 "I:\Microchip\xc8\v1.33\include\pic18f1320.h"
PREINC2 equ 0FDCh ;# 
# 3771 "I:\Microchip\xc8\v1.33\include\pic18f1320.h"
POSTDEC2 equ 0FDDh ;# 
# 3777 "I:\Microchip\xc8\v1.33\include\pic18f1320.h"
POSTINC2 equ 0FDEh ;# 
# 3783 "I:\Microchip\xc8\v1.33\include\pic18f1320.h"
INDF2 equ 0FDFh ;# 
# 3789 "I:\Microchip\xc8\v1.33\include\pic18f1320.h"
BSR equ 0FE0h ;# 
# 3795 "I:\Microchip\xc8\v1.33\include\pic18f1320.h"
FSR1 equ 0FE1h ;# 
# 3801 "I:\Microchip\xc8\v1.33\include\pic18f1320.h"
FSR1L equ 0FE1h ;# 
# 3807 "I:\Microchip\xc8\v1.33\include\pic18f1320.h"
FSR1H equ 0FE2h ;# 
# 3813 "I:\Microchip\xc8\v1.33\include\pic18f1320.h"
PLUSW1 equ 0FE3h ;# 
# 3819 "I:\Microchip\xc8\v1.33\include\pic18f1320.h"
PREINC1 equ 0FE4h ;# 
# 3825 "I:\Microchip\xc8\v1.33\include\pic18f1320.h"
POSTDEC1 equ 0FE5h ;# 
# 3831 "I:\Microchip\xc8\v1.33\include\pic18f1320.h"
POSTINC1 equ 0FE6h ;# 
# 3837 "I:\Microchip\xc8\v1.33\include\pic18f1320.h"
INDF1 equ 0FE7h ;# 
# 3843 "I:\Microchip\xc8\v1.33\include\pic18f1320.h"
WREG equ 0FE8h ;# 
# 3854 "I:\Microchip\xc8\v1.33\include\pic18f1320.h"
FSR0 equ 0FE9h ;# 
# 3860 "I:\Microchip\xc8\v1.33\include\pic18f1320.h"
FSR0L equ 0FE9h ;# 
# 3866 "I:\Microchip\xc8\v1.33\include\pic18f1320.h"
FSR0H equ 0FEAh ;# 
# 3872 "I:\Microchip\xc8\v1.33\include\pic18f1320.h"
PLUSW0 equ 0FEBh ;# 
# 3878 "I:\Microchip\xc8\v1.33\include\pic18f1320.h"
PREINC0 equ 0FECh ;# 
# 3884 "I:\Microchip\xc8\v1.33\include\pic18f1320.h"
POSTDEC0 equ 0FEDh ;# 
# 3890 "I:\Microchip\xc8\v1.33\include\pic18f1320.h"
POSTINC0 equ 0FEEh ;# 
# 3896 "I:\Microchip\xc8\v1.33\include\pic18f1320.h"
INDF0 equ 0FEFh ;# 
# 3902 "I:\Microchip\xc8\v1.33\include\pic18f1320.h"
INTCON3 equ 0FF0h ;# 
# 3993 "I:\Microchip\xc8\v1.33\include\pic18f1320.h"
INTCON2 equ 0FF1h ;# 
# 4069 "I:\Microchip\xc8\v1.33\include\pic18f1320.h"
INTCON equ 0FF2h ;# 
# 4219 "I:\Microchip\xc8\v1.33\include\pic18f1320.h"
PROD equ 0FF3h ;# 
# 4225 "I:\Microchip\xc8\v1.33\include\pic18f1320.h"
PRODL equ 0FF3h ;# 
# 4231 "I:\Microchip\xc8\v1.33\include\pic18f1320.h"
PRODH equ 0FF4h ;# 
# 4237 "I:\Microchip\xc8\v1.33\include\pic18f1320.h"
TABLAT equ 0FF5h ;# 
# 4245 "I:\Microchip\xc8\v1.33\include\pic18f1320.h"
TBLPTR equ 0FF6h ;# 
# 4251 "I:\Microchip\xc8\v1.33\include\pic18f1320.h"
TBLPTRL equ 0FF6h ;# 
# 4257 "I:\Microchip\xc8\v1.33\include\pic18f1320.h"
TBLPTRH equ 0FF7h ;# 
# 4263 "I:\Microchip\xc8\v1.33\include\pic18f1320.h"
TBLPTRU equ 0FF8h ;# 
# 4271 "I:\Microchip\xc8\v1.33\include\pic18f1320.h"
PCLAT equ 0FF9h ;# 
# 4278 "I:\Microchip\xc8\v1.33\include\pic18f1320.h"
PC equ 0FF9h ;# 
# 4284 "I:\Microchip\xc8\v1.33\include\pic18f1320.h"
PCL equ 0FF9h ;# 
# 4290 "I:\Microchip\xc8\v1.33\include\pic18f1320.h"
PCLATH equ 0FFAh ;# 
# 4296 "I:\Microchip\xc8\v1.33\include\pic18f1320.h"
PCLATU equ 0FFBh ;# 
# 4302 "I:\Microchip\xc8\v1.33\include\pic18f1320.h"
STKPTR equ 0FFCh ;# 
# 4407 "I:\Microchip\xc8\v1.33\include\pic18f1320.h"
TOS equ 0FFDh ;# 
# 4413 "I:\Microchip\xc8\v1.33\include\pic18f1320.h"
TOSL equ 0FFDh ;# 
# 4419 "I:\Microchip\xc8\v1.33\include\pic18f1320.h"
TOSH equ 0FFEh ;# 
# 4425 "I:\Microchip\xc8\v1.33\include\pic18f1320.h"
TOSU equ 0FFFh ;# 
	FNCALL	_main,_ConfigureOscillator
	FNCALL	_main,_InitApp
	FNCALL	_main,_UserMain
	FNROOT	_main
	FNCALL	intlevel2,_high_isr
	global	intlevel2
	FNROOT	intlevel2
	FNCALL	intlevel1,_low_isr
	global	intlevel1
	FNROOT	intlevel1
psect	intcodelo,class=CODE,space=0,reloc=2
global __pintcodelo
__pintcodelo:
; #config settings
global __CFG_OSC$HSPLL
__CFG_OSC$HSPLL equ 0x0
global __CFG_FSCM$OFF
__CFG_FSCM$OFF equ 0x0
global __CFG_IESO$OFF
__CFG_IESO$OFF equ 0x0
global __CFG_PWRT$ON
__CFG_PWRT$ON equ 0x0
global __CFG_BORV$45
__CFG_BORV$45 equ 0x0
global __CFG_BOR$OFF
__CFG_BOR$OFF equ 0x0
global __CFG_WDTPS$512
__CFG_WDTPS$512 equ 0x0
global __CFG_WDT$OFF
__CFG_WDT$OFF equ 0x0
global __CFG_MCLRE$ON
__CFG_MCLRE$ON equ 0x0
global __CFG_LVP$OFF
__CFG_LVP$OFF equ 0x0
global __CFG_STVR$OFF
__CFG_STVR$OFF equ 0x0
global __CFG_CP0$OFF
__CFG_CP0$OFF equ 0x0
global __CFG_CP1$OFF
__CFG_CP1$OFF equ 0x0
global __CFG_CPB$OFF
__CFG_CPB$OFF equ 0x0
global __CFG_CPD$OFF
__CFG_CPD$OFF equ 0x0
global __CFG_WRT0$OFF
__CFG_WRT0$OFF equ 0x0
global __CFG_WRT1$OFF
__CFG_WRT1$OFF equ 0x0
global __CFG_WRTB$OFF
__CFG_WRTB$OFF equ 0x0
global __CFG_WRTC$OFF
__CFG_WRTC$OFF equ 0x0
global __CFG_WRTD$OFF
__CFG_WRTD$OFF equ 0x0
global __CFG_EBTR0$OFF
__CFG_EBTR0$OFF equ 0x0
global __CFG_EBTR1$OFF
__CFG_EBTR1$OFF equ 0x0
global __CFG_EBTRB$OFF
__CFG_EBTRB$OFF equ 0x0
	file	"PWM_RGB.as"
	line	#
psect	cinit,class=CODE,delta=1,reloc=2
global __pcinit
__pcinit:
global start_initialization
start_initialization:

global __initialization
__initialization:
psect cinit,class=CODE,delta=1
global end_of_initialization,__end_of__initialization

;End of C runtime variable initialization code

end_of_initialization:
__end_of__initialization:movlb 0
goto _main	;jump to C main() function
psect	cstackCOMRAM,class=COMRAM,space=1,noexec
global __pcstackCOMRAM
__pcstackCOMRAM:
?_ConfigureOscillator:	; 0 bytes @ 0x0
?_InitApp:	; 0 bytes @ 0x0
?_UserMain:	; 0 bytes @ 0x0
?_high_isr:	; 0 bytes @ 0x0
?_low_isr:	; 0 bytes @ 0x0
??_low_isr:	; 0 bytes @ 0x0
?_main:	; 0 bytes @ 0x0
	ds   1
??_ConfigureOscillator:	; 0 bytes @ 0x1
??_InitApp:	; 0 bytes @ 0x1
??_UserMain:	; 0 bytes @ 0x1
??_high_isr:	; 0 bytes @ 0x1
??_main:	; 0 bytes @ 0x1
;!
;!Data Sizes:
;!    Strings     0
;!    Constant    0
;!    Data        0
;!    BSS         0
;!    Persistent  0
;!    Stack       0
;!
;!Auto Spaces:
;!    Space          Size  Autos    Used
;!    COMRAM          127      1       1
;!    BANK0           128      0       0

;!
;!Pointer List with Targets:
;!
;!    None.


;!
;!Critical Paths under _main in COMRAM
;!
;!    None.
;!
;!Critical Paths under _high_isr in COMRAM
;!
;!    None.
;!
;!Critical Paths under _low_isr in COMRAM
;!
;!    None.
;!
;!Critical Paths under _main in BANK0
;!
;!    None.
;!
;!Critical Paths under _high_isr in BANK0
;!
;!    None.
;!
;!Critical Paths under _low_isr in BANK0
;!
;!    None.

;;
;;Main: autosize = 0, tempsize = 0, incstack = 0, save=0
;;

;!
;!Call Graph Tables:
;!
;! ---------------------------------------------------------------------------------
;! (Depth) Function   	        Calls       Base Space   Used Autos Params    Refs
;! ---------------------------------------------------------------------------------
;! (0) _main                                                 0     0      0       0
;!                _ConfigureOscillator
;!                            _InitApp
;!                           _UserMain
;! ---------------------------------------------------------------------------------
;! (1) _UserMain                                             0     0      0       0
;! ---------------------------------------------------------------------------------
;! (1) _InitApp                                              0     0      0       0
;! ---------------------------------------------------------------------------------
;! (1) _ConfigureOscillator                                  0     0      0       0
;! ---------------------------------------------------------------------------------
;! Estimated maximum stack depth 1
;! ---------------------------------------------------------------------------------
;! (Depth) Function   	        Calls       Base Space   Used Autos Params    Refs
;! ---------------------------------------------------------------------------------
;! (2) _low_isr                                              1     1      0       0
;!                                              0 COMRAM     1     1      0
;! ---------------------------------------------------------------------------------
;! Estimated maximum stack depth 2
;! ---------------------------------------------------------------------------------
;! (Depth) Function   	        Calls       Base Space   Used Autos Params    Refs
;! ---------------------------------------------------------------------------------
;! (3) _high_isr                                             0     0      0       0
;! ---------------------------------------------------------------------------------
;! Estimated maximum stack depth 3
;! ---------------------------------------------------------------------------------
;!
;! Call Graph Graphs:
;!
;! _main (ROOT)
;!   _ConfigureOscillator
;!   _InitApp
;!   _UserMain
;!
;! _low_isr (ROOT)
;!
;! _high_isr (ROOT)
;!

;! Address spaces:

;!Name               Size   Autos  Total    Cost      Usage
;!BITCOMRAM           7F      0       0       0        0.0%
;!EEDATA             100      0       0       0        0.0%
;!NULL                 0      0       0       0        0.0%
;!CODE                 0      0       0       0        0.0%
;!COMRAM              7F      1       1       1        0.8%
;!STACK                0      0       0       2        0.0%
;!BITBANK0            80      0       0       3        0.0%
;!BANK0               80      0       0       4        0.0%
;!ABS                  0      0       0       5        0.0%
;!BIGRAM              FF      0       0       6        0.0%
;!DATA                 0      0       0       7        0.0%
;!BITSFR               0      0       0      40        0.0%
;!SFR                  0      0       0      40        0.0%

	global	_main

;; *************** function _main *****************
;; Defined at:
;;		line 33 in file "H:\Razrab\PIC\work\PWM_RGB\main.c"
;; Parameters:    Size  Location     Type
;;		None
;; Auto vars:     Size  Location     Type
;;		None
;; Return value:  Size  Location     Type
;;		None               void
;; Registers used:
;;		cstack
;; Tracked objects:
;;		On entry : 0/0
;;		On exit  : 0/0
;;		Unchanged: 0/0
;; Data sizes:     COMRAM   BANK0
;;      Params:         0       0
;;      Locals:         0       0
;;      Temps:          0       0
;;      Totals:         0       0
;;Total ram usage:        0 bytes
;; Hardware stack levels required when called:    3
;; This function calls:
;;		_ConfigureOscillator
;;		_InitApp
;;		_UserMain
;; This function is called by:
;;		Startup code after reset
;; This function uses a non-reentrant model
;;
psect	text0,class=CODE,space=0,reloc=2
	file	"H:\Razrab\PIC\work\PWM_RGB\main.c"
	line	33
global __ptext0
__ptext0:
psect	text0
	file	"H:\Razrab\PIC\work\PWM_RGB\main.c"
	line	33
	global	__size_of_main
	__size_of_main	equ	__end_of_main-_main
	
_main:
;incstack = 0
	opt	stack 28
	line	36
	
l603:
;main.c: 36: ConfigureOscillator();
	call	_ConfigureOscillator	;wreg free
	line	40
;main.c: 40: InitApp();
	call	_InitApp	;wreg free
	line	46
;main.c: 46: while(1)
	
l15:
	line	48
;main.c: 47: {
;main.c: 48: UserMain();
	call	_UserMain	;wreg free
	goto	l15
	global	start
	goto	start
	opt stack 0
	line	50
GLOBAL	__end_of_main
	__end_of_main:
	signat	_main,88
	global	_UserMain

;; *************** function _UserMain *****************
;; Defined at:
;;		line 44 in file "H:\Razrab\PIC\work\PWM_RGB\user.c"
;; Parameters:    Size  Location     Type
;;		None
;; Auto vars:     Size  Location     Type
;;		None
;; Return value:  Size  Location     Type
;;		None               void
;; Registers used:
;;		None
;; Tracked objects:
;;		On entry : 0/0
;;		On exit  : 0/0
;;		Unchanged: 0/0
;; Data sizes:     COMRAM   BANK0
;;      Params:         0       0
;;      Locals:         0       0
;;      Temps:          0       0
;;      Totals:         0       0
;;Total ram usage:        0 bytes
;; Hardware stack levels used:    1
;; Hardware stack levels required when called:    2
;; This function calls:
;;		Nothing
;; This function is called by:
;;		_main
;; This function uses a non-reentrant model
;;
psect	text1,class=CODE,space=0,reloc=2
	file	"H:\Razrab\PIC\work\PWM_RGB\user.c"
	line	44
global __ptext1
__ptext1:
psect	text1
	file	"H:\Razrab\PIC\work\PWM_RGB\user.c"
	line	44
	global	__size_of_UserMain
	__size_of_UserMain	equ	__end_of_UserMain-_UserMain
	
_UserMain:
;incstack = 0
	opt	stack 28
	line	47
	
l24:
	return
	opt stack 0
GLOBAL	__end_of_UserMain
	__end_of_UserMain:
	signat	_UserMain,88
	global	_InitApp

;; *************** function _InitApp *****************
;; Defined at:
;;		line 9 in file "H:\Razrab\PIC\work\PWM_RGB\user.c"
;; Parameters:    Size  Location     Type
;;		None
;; Auto vars:     Size  Location     Type
;;		None
;; Return value:  Size  Location     Type
;;		None               void
;; Registers used:
;;		None
;; Tracked objects:
;;		On entry : 0/0
;;		On exit  : 0/0
;;		Unchanged: 0/0
;; Data sizes:     COMRAM   BANK0
;;      Params:         0       0
;;      Locals:         0       0
;;      Temps:          0       0
;;      Totals:         0       0
;;Total ram usage:        0 bytes
;; Hardware stack levels used:    1
;; Hardware stack levels required when called:    2
;; This function calls:
;;		Nothing
;; This function is called by:
;;		_main
;; This function uses a non-reentrant model
;;
psect	text2,class=CODE,space=0,reloc=2
	line	9
global __ptext2
__ptext2:
psect	text2
	file	"H:\Razrab\PIC\work\PWM_RGB\user.c"
	line	9
	global	__size_of_InitApp
	__size_of_InitApp	equ	__end_of_InitApp-_InitApp
	
_InitApp:
;incstack = 0
	opt	stack 28
	line	42
	
l21:
	return
	opt stack 0
GLOBAL	__end_of_InitApp
	__end_of_InitApp:
	signat	_InitApp,88
	global	_ConfigureOscillator

;; *************** function _ConfigureOscillator *****************
;; Defined at:
;;		line 24 in file "H:\Razrab\PIC\work\PWM_RGB\system.c"
;; Parameters:    Size  Location     Type
;;		None
;; Auto vars:     Size  Location     Type
;;		None
;; Return value:  Size  Location     Type
;;		None               void
;; Registers used:
;;		None
;; Tracked objects:
;;		On entry : 0/0
;;		On exit  : 0/0
;;		Unchanged: 0/0
;; Data sizes:     COMRAM   BANK0
;;      Params:         0       0
;;      Locals:         0       0
;;      Temps:          0       0
;;      Totals:         0       0
;;Total ram usage:        0 bytes
;; Hardware stack levels used:    1
;; Hardware stack levels required when called:    2
;; This function calls:
;;		Nothing
;; This function is called by:
;;		_main
;; This function uses a non-reentrant model
;;
psect	text3,class=CODE,space=0,reloc=2
	file	"H:\Razrab\PIC\work\PWM_RGB\system.c"
	line	24
global __ptext3
__ptext3:
psect	text3
	file	"H:\Razrab\PIC\work\PWM_RGB\system.c"
	line	24
	global	__size_of_ConfigureOscillator
	__size_of_ConfigureOscillator	equ	__end_of_ConfigureOscillator-_ConfigureOscillator
	
_ConfigureOscillator:
;incstack = 0
	opt	stack 28
	line	33
	
l27:
	return
	opt stack 0
GLOBAL	__end_of_ConfigureOscillator
	__end_of_ConfigureOscillator:
	signat	_ConfigureOscillator,88
	global	_low_isr

;; *************** function _low_isr *****************
;; Defined at:
;;		line 38 in file "H:\Razrab\PIC\work\PWM_RGB\interrupts.c"
;; Parameters:    Size  Location     Type
;;		None
;; Auto vars:     Size  Location     Type
;;		None
;; Return value:  Size  Location     Type
;;		None               void
;; Registers used:
;;		None
;; Tracked objects:
;;		On entry : 0/0
;;		On exit  : 0/0
;;		Unchanged: 0/0
;; Data sizes:     COMRAM   BANK0
;;      Params:         0       0
;;      Locals:         0       0
;;      Temps:          1       0
;;      Totals:         1       0
;;Total ram usage:        1 bytes
;; Hardware stack levels used:    1
;; Hardware stack levels required when called:    1
;; This function calls:
;;		Nothing
;; This function is called by:
;;		Interrupt level 1
;; This function uses a non-reentrant model
;;
psect	intcodelo
psect	intcodelo
	file	"H:\Razrab\PIC\work\PWM_RGB\interrupts.c"
	line	38
	global	__size_of_low_isr
	__size_of_low_isr	equ	__end_of_low_isr-_low_isr
	
_low_isr:
;incstack = 0
	opt	stack 28
	movff	bsr+0,??_low_isr+0
	line	47
	
i1l6:
	movff	??_low_isr+0,bsr+0
	retfie
	opt stack 0
GLOBAL	__end_of_low_isr
	__end_of_low_isr:
	signat	_low_isr,1112
	global	_high_isr

;; *************** function _high_isr *****************
;; Defined at:
;;		line 25 in file "H:\Razrab\PIC\work\PWM_RGB\interrupts.c"
;; Parameters:    Size  Location     Type
;;		None
;; Auto vars:     Size  Location     Type
;;		None
;; Return value:  Size  Location     Type
;;		None               void
;; Registers used:
;;		None
;; Tracked objects:
;;		On entry : 0/0
;;		On exit  : 0/0
;;		Unchanged: 0/0
;; Data sizes:     COMRAM   BANK0
;;      Params:         0       0
;;      Locals:         0       0
;;      Temps:          0       0
;;      Totals:         0       0
;;Total ram usage:        0 bytes
;; Hardware stack levels used:    1
;; This function calls:
;;		Nothing
;; This function is called by:
;;		Interrupt level 2
;; This function uses a non-reentrant model
;;
psect	intcode,class=CODE,space=0,reloc=2
global __pintcode
__pintcode:
psect	intcode
	file	"H:\Razrab\PIC\work\PWM_RGB\interrupts.c"
	line	25
	global	__size_of_high_isr
	__size_of_high_isr	equ	__end_of_high_isr-_high_isr
	
_high_isr:
;incstack = 0
	opt	stack 28
	global	int_func
	goto	int_func
psect	intcode_body,class=CODE,space=0,reloc=2
global __pintcode_body
__pintcode_body:
int_func:
	line	34
	
i2l3:
	retfie f
	opt stack 0
GLOBAL	__end_of_high_isr
	__end_of_high_isr:
	signat	_high_isr,88
	GLOBAL	__activetblptr
__activetblptr	EQU	0
	psect	intsave_regs,class=BIGRAM,space=1,noexec
	PSECT	rparam,class=COMRAM,space=1,noexec
	GLOBAL	__Lrparam
	FNCONF	rparam,??,?
GLOBAL	__Lparam, __Hparam
GLOBAL	__Lrparam, __Hrparam
__Lparam	EQU	__Lrparam
__Hparam	EQU	__Hrparam
	end
