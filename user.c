/******************************************************************************/
/*Files to Include                                                            */
/******************************************************************************/

#if defined(__XC)
    #include <xc.h>         /* XC8 General Include File */
#elif defined(HI_TECH_C)
    #include <htc.h>        /* HiTech General Include File */
#elif defined(__18CXX)
    #include <p18cxxx.h>    /* C18 General Include File */
#endif

#if defined(__XC) || defined(HI_TECH_C)

#include <stdint.h>         /* For uint8_t definition */
#include <stdbool.h>        /* For true/false definition */

#endif

#include "config.h"
#include "softPWM.h"
#include "shaduler.h"
#include "buttons.h"
#include "valcoder.h"
#include "port.h"

#include "user.h"

enum Colors {
    RED = 0,
    GREEN = 1,
    BLUE = 2,
};

/******************************************************************************/
/* User Data                                                                  */
/******************************************************************************/

__EEPROM_DATA(50, 50, 50, 0, 0, 0, 0, 0); // startup colors

static struct SoftPWMEntry softPWMData[3] = {
    {
        100, 0, 0, SetR
    },
    {
        100, 0, 0, SetG
    },
    {
        100, 0, 0, SetB
    }
};

static struct Button button = {getButtonState, };

static struct Valcoder valcoder = { getValcoderState, };

static uint16_t dimmer_setup_active = 0;

/******************************************************************************/
/* User Functions                                                             */
/******************************************************************************/
////////////////////////////////////////////////////////////////////////////////

static void dimmer_reset_idle_timer() {
    dimmer_setup_active = SETUP_IDLE_TIME_MS;
}

static void dimmer_next_color(enum ButtonActionID, void* user_data) {
    dimmer_reset_idle_timer();

    struct SoftPWMEntry* PWM_data = user_data;

    valcoder.user_data = PWM_data;
    if (++PWM_data == &softPWMData[3]) {
        PWM_data = &softPWMData[0];
    }
    button.user_data = PWM_data;
}

static void dimmer_p1(void* user_data) {
    dimmer_reset_idle_timer();

    struct SoftPWMEntry* PWM_data = user_data;
    if (PWM_data->value < PWM_data->period)
        ++PWM_data->value;

#ifdef __DEBUG
    valcoder_tp(NULL);
#endif
}

static void dimmer_m1(void* user_data) {
    dimmer_reset_idle_timer();

    struct SoftPWMEntry* PWM_data = user_data;
    if (PWM_data->value > 0)
        --PWM_data->value;
#ifdef __DEBUG
    valcoder_tm(NULL);
#endif
}

static void dimmer_enter_setup(enum ButtonActionID, void* user_data) {
    dimmer_reset_idle_timer();

    button.on_long_push = NULL;
    button.user_data = &softPWMData[GREEN];
    button.on_push = dimmer_next_color;

    valcoder.on_minus1 = dimmer_m1;
    valcoder.on_plus1 = dimmer_p1;
    valcoder.user_data = &softPWMData[RED];

#ifdef __DEBUG
    btnTest_r(0, NULL);
#endif
}

static void dimmer_setup_commit() {
    // save PWM values to EEPROM
    eeprom_write(RED, softPWMData[RED].value);
    eeprom_write(GREEN, softPWMData[GREEN].value);
    eeprom_write(BLUE, softPWMData[BLUE].value);
}

static void dimmer_setup_reset() {
    // reset
    button.on_click = NULL;
    button.on_push = NULL;
    button.on_release = NULL;
    button.on_long_push = dimmer_enter_setup;
    button.user_data = NULL;

    valcoder.on_minus1 = NULL;
    valcoder.on_plus1 = NULL;
    valcoder.user_data = NULL;

    // load settings
    softPWMData[RED].value = eeprom_read(RED);
    softPWMData[GREEN].value = eeprom_read(GREEN);
    softPWMData[BLUE].value = eeprom_read(BLUE);
}

static void dimmer_tick() {
    if (dimmer_setup_active) {
        --dimmer_setup_active;
        if (!dimmer_setup_active) {
            dimmer_setup_commit();
            dimmer_setup_reset();
        }
    }
}

////////////////////////////////////////////////////////////////////////////////

static void RegisterPWMPins() {
    SoftPWM_addPin(&softPWMData[RED]);
    SoftPWM_addPin(&softPWMData[GREEN]);
    SoftPWM_addPin(&softPWMData[BLUE]);
    
    SoftPWM_configure(SOFT_PWM_TIMER_CLOCK / 255 / SOFT_PWM_BASE_FREQ);
}

static void RegisterButtons() {
    buttons_register(&button);

#ifdef __DEBUG
    //button.on_push = btnTest_r;
    //button.on_release = btnTest_r;
    //button.on_click = btnTest_r;
    //button.on_long_push = btnTest_r;
#endif
}

static void RegisterValcoders() {
    valcoder_register(&valcoder);

#ifdef __DEBUG
    //valcoder.on_minus1 = valcoder_tm;
    //valcoder.on_plus1 = valcoder_tp;
#endif
}

static void RegisterTasks() {
    Shaduler_register_task(valcoder_process, 1);
    Shaduler_register_task(buttons_process, 100);
    Shaduler_register_task(dimmer_tick, 1);
}

void InitApp(void)
{
    DisableUnnessusaryModules();

    ConfigureIOPins();
    RegisterButtons();
    RegisterValcoders();
    RegisterTasks();

    dimmer_setup_reset(); // reset dimmer setup & load settings
    
    RegisterPWMPins();
    
    ShadulerEnable(SHADULER_TIMER_CLOCK / 1000);
}

void UserMain()
{
    Shaduler_Process();
}
