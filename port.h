/* 
 * File:   port.h
 * Author: tolyan
 *
 * Created on 12 ??????? 2016 ?., 8:16
 */

#ifndef PORT_H
#define	PORT_H

#include "buttons.h"

#ifdef	__cplusplus
extern "C" {
#endif

#define OUTPUT_ACTIVE_LVL   (FALSE)

#if OUTPUT_ACTIVE_LVL
#define PIN_SET(pin, v)          if (v) { (pin) = TRUE; } else { (pin) = FALSE; }
#else
#define PIN_SET(pin, v)          if (v) { (pin) = FALSE; } else { (pin) = TRUE; }
#endif

#if defined(__18F1320__) || defined(__18F252__)
#define SOFT_PWM_TIMER_CLOCK    (FCY)
#define SHADULER_TIMER_CLOCK    (FCY)
#endif

#if defined(__18F1320__) || defined(__18F252__)
#define G_ENABLE_INTERRUPTS()   \
do { \
    /* Configure the IPEN bit (1=on) in RCON to turn on/off int priorities */ \
    RCONbits.IPEN = TRUE; \
    /* Enable interrupts */ \
    INTCONbits.GIEH = TRUE; \
    INTCONbits.GIEL = TRUE; \
} while (0)
    
#define G_DISABLE_INTERRUPTS()  \
do { \
    /* Disable interrupts */ \
    INTCONbits.GIEL = FALSE; \
    INTCONbits.GIEH = FALSE; \
} while (0)
#endif

    typedef void (*shaduler_tick_routine)();
    typedef void (*soft_pwm_routine)();
    
    void DisableUnnessusaryModules();
    void ConfigureIOPins();

    void SetR(char value);
    void SetG(char value);
    void SetB(char value);

    BOOL getButtonState();
    char getValcoderState();

    void btnTest_r(enum ButtonActionID, void*);
    void valcoder_tp(void*);
    void valcoder_tm(void*);

    void configure_shaduler_timer(uint32_t devider, shaduler_tick_routine r);
    void configure_soft_pwm_timer(uint32_t devider, soft_pwm_routine r);

    void ConfigureOscillator();

#ifdef	__cplusplus
}
#endif

#endif	/* PORT_H */

