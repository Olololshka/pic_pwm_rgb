/******************************************************************************/
/*Files to Include                                                            */
/******************************************************************************/

#if defined(__XC)
    #include <xc.h>         /* XC8 General Include File */
#elif defined(HI_TECH_C)
    #include <htc.h>        /* HiTech General Include File */
#elif defined(__18CXX)
    #include <p18cxxx.h>    /* C18 General Include File */
#endif

#if defined(__XC) || defined(HI_TECH_C)

#include <stdint.h>         /* For uint8_t definition */
#include <stdbool.h>        /* For true/false definition */

#endif

#include <stdlib.h>

#include "port.h"
#include "config.h"
#include "softPWM.h"

/* USE Timer 3 */

////////////////////////////////////////////////////////////////////////////////

static struct SoftPWMEntry* softPWMs[MAX_SOFT_PWMS];

////////////////////////////////////////////////////////////////////////////////

void SoftPWMISR() {
    unsigned char i;
    for (i = 0; i < MAX_SOFT_PWMS; ++i) {
        struct SoftPWMEntry* current = softPWMs[i];
        if (current && current->setPin) {
            if (current->value == current->period) {
                current->setPin(TRUE);
                continue;
            }

			if (current->value == 0) {
				current->setPin(FALSE);
                continue;
			}

            if (current->counter) {
                --current->counter;
                if (current->counter == current->value) {
                    current->setPin(TRUE);
                }
            }
            else {
                current->counter = current->period;
                current->setPin(FALSE);
            }
        }
    }
}

void SoftPWM_configure(uint32_t targetBasePeriod) {
    configure_soft_pwm_timer(targetBasePeriod, SoftPWMISR);
}

error_t SoftPWM_addPin(struct SoftPWMEntry* entry) {
    unsigned char i;
    for (i = 0; i < MAX_SOFT_PWMS; ++i) {
        if (softPWMs[i] == NULL) {
            softPWMs[i] = entry;
            return OK;
        }
    }
    return ERROR;
}

error_t SoftPWM_rmPin(struct SoftPWMEntry* entry) {
    unsigned char i;
    for (i = 0; i < MAX_SOFT_PWMS; ++i) {
        if (softPWMs[i] == entry) {
            softPWMs[i] = NULL;
            return OK;
        }
    }
    return ERROR;
}