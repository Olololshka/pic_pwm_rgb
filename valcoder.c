/******************************************************************************/
/*Files to Include                                                            */
/******************************************************************************/

#if defined(__XC)
    #include <xc.h>         /* XC8 General Include File */
#elif defined(HI_TECH_C)
    #include <htc.h>        /* HiTech General Include File */
#elif defined(__18CXX)
    #include <p18cxxx.h>    /* C18 General Include File */
#endif

#if defined(__XC) || defined(HI_TECH_C)

#include <stdint.h>         /* For uint8_t definition */
#include <stdbool.h>        /* For true/false definition */

#endif

#include <stdlib.h>

#include "config.h"
#include "valcoder.h"

#define VC_PIN1         PORTBbits.RB2
#define VC_PIN2         PORTBbits.RB3

// this table works both ways
// state number -> state value
// state value -> state number
static const signed char valcoder_table[] = {
    0b00, 0b01, 0b11, 0b10
};

static struct Valcoder *valcoders[MAX_VALCODERS];

////////////////////////////////////////////////////////////////////////////////

BOOL valcoder_register(struct Valcoder* valcoder) {
    unsigned char i;
    for (i = 0; i < MAX_VALCODERS; ++i) {
        if (valcoders[i] == NULL) {
            valcoders[i] = valcoder;
            return TRUE;
        }
    }
    return FALSE;
}

BOOL valcoder_unregister(struct Valcoder* valcoder) {
    unsigned char i;
    for (i = 0; i < MAX_VALCODERS; ++i) {
        if (valcoders[i] == valcoder) {
            valcoders[i] = NULL;
            return TRUE;
        }
    }
    return FALSE;
}

void valcoder_process() {
    unsigned char i;
    for (i = 0; i < MAX_VALCODERS; ++i) {
        struct Valcoder* v = valcoders[i];
        if (v) {
            char new_state = v->getter();
            if (v->prev_state != new_state) {
                 // substract state numbers to get direction
                // overflow detection
                switch (valcoder_table[new_state] -
                        valcoder_table[v->prev_state]) {
                    case 1: // forward
                    case -3: // forward overflow
                        if (v->on_plus1) {
                            v->on_plus1(v->user_data);
                        }
                        break;
                    case -1: // backword
                    case 3: // backword overflow
                        if (v->on_minus1) {
                            v->on_minus1(v->user_data);
                        }
                        break;
                }
                v->prev_state = new_state;
            }
        }
    }
}