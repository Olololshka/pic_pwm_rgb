/******************************************************************************/
/*Files to Include                                                            */
/******************************************************************************/

#if defined(__XC)
    #include <xc.h>         /* XC8 General Include File */
#elif defined(HI_TECH_C)
    #include <htc.h>        /* HiTech General Include File */
#elif defined(__18CXX)
    #include <p18cxxx.h>    /* C18 General Include File */
#endif

#if defined(__XC) || defined(HI_TECH_C)

#include <stdint.h>         /* For uint8_t definition */
#include <stdbool.h>        /* For true/false definition */

#endif


#include "config.h"
#include "buttons.h"

#define LONG_PUSH_MASK          ((uint8_t)((1 << LONG_PUSH_TICKS) - 1))

static struct Button* buttons[MAX_BUTTONS];

BOOL buttons_register(struct Button* btn) {
    unsigned char i;
    for (i = 0; i < MAX_BUTTONS; ++i) {
        if (buttons[i] == NULL) {
            buttons[i] = btn;
            return TRUE;
        }
    }
    return FALSE;
}

BOOL buttons_unregister(struct Button* btn) {
    unsigned char i;
    for (i = 0; i < MAX_BUTTONS; ++i) {
        if (buttons[i] == btn) {
            buttons[i] = NULL;
            return TRUE;
        }
    }
    return FALSE;
}

void buttons_process() {
    unsigned char i;
    for (i = 0; i < MAX_BUTTONS; ++i) {
        struct Button *b = buttons[i];
        if (b) {
            BOOL newVal = b->getter();
            unsigned char prevstates = b->prevstates;
            
            if (newVal != (prevstates & 1)) {
                switch (prevstates & 0b11) {
                    case 0b00: // push
                        if (b->on_push)
                            b->on_push(BA_PUSH, b->user_data);
                        break;
                    case 0b11: // release
                        if (b->on_release)
                            b->on_release(BA_RELEASE, b->user_data);
                        if (!b->long_push_detected && b->on_click)
                            b->on_click(BA_CLICK, b->user_data);
                        break;
                }
                b->long_push_detected = FALSE;
                prevstates = (prevstates << 1) | newVal;
            } else {
                prevstates = (prevstates << 1) | newVal;
                if (newVal && !b->long_push_detected  &&
                        (prevstates & LONG_PUSH_MASK == prevstates)) {
                    //prevstates &= ~1;
                    b->long_push_detected = TRUE;
                    if (b->on_long_push)
                        b->on_long_push(BA_LONG_PUSH, b->user_data);
                }
            }
            b->prevstates = prevstates;
        }
    }
}