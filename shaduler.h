/* 
 * File:   shaduler.h
 * Author: tolyan
 *
 * Created on 11 ???? 2015 ?., 17:05
 */

#ifndef SHADULER_H
#define	SHADULER_H

#ifdef	__cplusplus
extern "C" {
#endif

    typedef void (*taskRoutine)(void);

    BOOL Shaduler_register_task(taskRoutine routine, uint16_t period);
    BOOL Shaduler_unregister_task(taskRoutine routine, BOOL all);
    void ShadulerEnable(uint32_t targetBasePeriod);
    void ShadulerTick();
    void Shaduler_Process();
    BOOL Shadule(taskRoutine routine);
    BOOL Shadule_ISR(taskRoutine routine);

#ifdef	__cplusplus
}
#endif

#endif	/* SHADULER_H */

