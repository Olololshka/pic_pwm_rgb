/* 
 * File:   buttons.h
 * Author: tolyan
 *
 * Created on 10 ??????? 2016 ?., 13:04
 */

#ifndef BUTTONS_H
#define	BUTTONS_H

#ifdef	__cplusplus
extern "C" {
#endif
    
    enum ButtonActionID {
        BA_PUSH,
        BA_RELEASE,
        BA_CLICK,
        BA_LONG_PUSH
    };

    typedef BOOL (*get_btn_state_cb)();
    typedef void (*button_action)(enum ButtonActionID ActionID, void* user_data);

    struct Button {
        get_btn_state_cb getter;
        button_action on_push;
        button_action on_release;
        button_action on_click;
        button_action on_long_push;
        
        unsigned long_push_detected:1;
        unsigned char prevstates;
        void* user_data;
    };

    BOOL buttons_register(struct Button* btn);
    BOOL buttons_unregister(struct Button* btn);
    void buttons_process();

#ifdef	__cplusplus
}
#endif

#endif	/* BUTTONS_H */

