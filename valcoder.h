/* 
 * File:   vocoder.h
 * Author: tolyan
 *
 * Created on 9 ??????? 2016 ?., 16:14
 */

#ifndef VALCODER_H
#define	VALCODER_H

#ifdef	__cplusplus
extern "C" {
#endif

    typedef void (*valcoder_callback)(void* user_data);
    typedef char (*valcoder_getter)();

    struct Valcoder {
        valcoder_getter getter;
        char prev_state;
        valcoder_callback on_plus1;
        valcoder_callback on_minus1;
        void* user_data;
    };

    BOOL valcoder_register(struct Valcoder* valcoder);
    BOOL valcoder_unregister(struct Valcoder* valcoder);
    void valcoder_process();

#ifdef	__cplusplus
}
#endif

#endif	/* VALCODER_H */

