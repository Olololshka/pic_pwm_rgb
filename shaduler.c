/******************************************************************************/
/*Files to Include                                                            */
/******************************************************************************/

#if defined(__XC)
    #include <xc.h>         /* XC8 General Include File */
#elif defined(HI_TECH_C)
    #include <htc.h>        /* HiTech General Include File */
#elif defined(__18CXX)
    #include <p18cxxx.h>    /* C18 General Include File */
#endif

#if defined(__XC) || defined(HI_TECH_C)

#include <stdint.h>         /* For uint8_t definition */
#include <stdbool.h>        /* For true/false definition */

#endif

#include <string.h>

#include "config.h"
#include "port.h"

#include "shaduler.h"

#define MIN(x, y)	((x < y) ? x : y)

// using TIMER2 for shaduling on 1ms

struct Task
{
    taskRoutine Routine;
    uint16_t period;
};

static struct Task tasks[MAX_TASKS];
static struct Task tasks_work[MAX_TASKS];

static uint8_t taskQueue_wp = 0;
static uint8_t taskQueue_rp = 0;
static uint8_t taskQueue_used = 0;
static taskRoutine taskQueue[TASK_QUEUE_SIZE];

static void reloadTask(uint8_t id)
{
    if (tasks[id].Routine) {
        memcpy(&tasks_work[id], & tasks[id], sizeof(struct Task));
    } else {
        memset(&tasks_work[id], 0, sizeof(struct Task));
    }
}

BOOL Shaduler_register_task(taskRoutine routine, uint16_t period) {
    unsigned char i;
    for (i = 0; i < MAX_TASKS; ++i) {
        struct Task* t = &tasks[i];
        if (t->Routine == NULL) {
            t->period = period;
            t->Routine = routine;
            reloadTask(i);
            return TRUE;
        }
    }
    return FALSE;
}

BOOL Shaduler_unregister_task(taskRoutine routine, BOOL all) {
    unsigned char i;
    BOOL status = FALSE;
    for (i = 0; i < MAX_TASKS; ++i) {
        struct Task* t = &tasks[i];
        if (t->Routine == routine) {
            t->Routine = NULL;
            status = TRUE;
            reloadTask(i);
            if (!all)
                break;
        }
    }
    return status;
}

void ShadulerEnable(uint32_t targetBasePeriod)
{
    configure_shaduler_timer(targetBasePeriod, ShadulerTick);
}

void ShadulerTick()
{    
    for (uint8_t TaskID = 0; TaskID < sizeof(tasks)/sizeof(struct Task);
            ++TaskID)
    {
        taskRoutine Routine = tasks_work[TaskID].Routine;
        if (Routine && (--tasks_work[TaskID].period == 0))
        {
            tasks_work[TaskID].period = tasks[TaskID].period; // reload
            Shadule_ISR(Routine);
        }
    }
}

void Shaduler_Process()
{
    while (taskQueue_used)
    {
        CLRWDT();
        taskRoutine shaduledRoutine = taskQueue[taskQueue_rp++];
        if (taskQueue_rp == sizeof(taskQueue) / sizeof(taskRoutine))
            taskQueue_rp = 0;
        --taskQueue_used;
        shaduledRoutine();
    }
}

BOOL Shadule(taskRoutine routine)
{
    BOOL res;

    if (taskQueue_used == sizeof(taskQueue) / sizeof(taskRoutine))
        res = FALSE;
    else
    {
        taskQueue[taskQueue_wp++] = routine;
        if (taskQueue_wp == sizeof(taskQueue) / sizeof(taskRoutine))
            taskQueue_wp = 0;

        ++taskQueue_used;

        res = TRUE;
    }

    return res;
}

BOOL Shadule_ISR(taskRoutine routine)
{
    BOOL res;

    if (taskQueue_used == sizeof(taskQueue) / sizeof(taskRoutine))
        res = FALSE;
    else
    {
        taskQueue[taskQueue_wp++] = routine;
        if (taskQueue_wp == sizeof(taskQueue) / sizeof(taskRoutine))
            taskQueue_wp = 0;

        ++taskQueue_used;

        res = TRUE;
    }

    return res;
}
